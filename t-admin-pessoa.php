<?php

use Hcode\Model;
use Hcode\Model\Auxiliar;
use Hcode\Model\Endereco;
use Hcode\Model\Pessoa;
use \Hcode\Model\User;
use \Hcode\Model\Telefone;
use Hcode\PageAdmin;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/pessoa/perfil', function() {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get($_SESSION[User::SESSION]['UseId']);

    $page = new PageAdmin();

    $page->setTpl("perfil", array(
        "oUser"=>$oUser->getValues(),
        "vError"=>Model::getError()
    ));

});

$app->get('/pessoa/perfil/atualizacao', function() {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get($_SESSION[User::SESSION]["UseId"]);

    $oEndereco = Endereco::enderecoPorPessoa($oUser->getPesId());

    if (!$oEndereco){
        $oEndereco = ["EndId"=>'',"EndRua"=>'',"EndNumero"=>'',"EndComplemento"=>'',"EndBairro"=>'',"EndCidade"=>'',"EndEstado"=>'',"EndPais"=>'',"EndCep"=>''];
    }

    $voTelefone = Telefone::telefonePorPessoa($oUser->getPesId());

    $voTelefoneTipo = Telefone::listTelefoneTipo();

    $voEstadoCivil = Auxiliar::listEstadoCivil();

    $voProfissao = Auxiliar::listProfissao();

    $voEstado = Auxiliar::listEstado();

    $voCidade = Auxiliar::listCidadePorEstado($oUser->getPfiUfNatural());

    $voEscolaridade = Auxiliar::listEscolaridade();

    $page = new PageAdmin();

    $page->setTpl("pessoa-atualizacao", array(
        "oUser"=>$oUser->getValues(),
        "voTelefone"=>$voTelefone,
        "voTelefoneTipo"=>$voTelefoneTipo,
        "oEndereco"=>$oEndereco,
        "voEscolaridade"=>$voEscolaridade,
        "voEstadoCivil"=>$voEstadoCivil,
        "voCidade"=>$voCidade,
        "voEstado"=>$voEstado,
        "voProfissao"=>$voProfissao,
        "vError"=>Model::getError()
    ));

});

$app->get('/pessoa/perfil/telefone/:nPesId', function($nPesId) {

    $voTelefone = Telefone::telefonePorPessoa($nPesId);
    $voTelefoneTipo = Telefone::listTipo();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("pessoa-telefone", array(
        "voTelefone"=>$voTelefone,
        "voTelefoneTipo"=>$voTelefoneTipo
    ));

});

$app->get('/pessoa/perfil/telefone/:nTelId/delete', function($nTelId) {

    $oTelefone = new Telefone();

    $oTelefone->get($nTelId);

    $oTelefone->delete();

});

$app->get('/pessoa/perfil/foto/:nUseId', function($nUseId) {

    User::verifyLogin();

    $oFoto = Pessoa::fotoPessoa($nUseId);

    if (!$oFoto){
        $oFoto['FotUrl'] = '\uploads\users\no-image.jpg';
        $oFoto['FotId'] = '';
    }

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("pessoa-foto", array(
        "oFoto"=>$oFoto
    ));

});

$app->post('/pessoa/perfil/foto', function() {

    User::verifyLogin();

    $oPessoa = new Pessoa();

    $oPessoa->updatePerfilFoto($_FILES['fFoto'],$_POST['PesId'],$_POST['FotId']);

});

$app->get('/pessoa/perfil/pessoal/:nPesId', function($nUseId) {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get($nUseId);

    $voTelefone = Telefone::telefonePorPessoa($oUser->getPesId());

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("pessoa-pessoal", array(
        "oUser"=>$oUser->getValues(),
        "voTelefone"=>$voTelefone,
        "sError"=>Model::getError()
    ));

});

$app->post('/pessoa/perfil/atualizacao', function() {

    $oPessoa = new Pessoa();

    $oPessoa->setData($_POST);

    try{

        $oPessoa->update();
        Model::setError("Atualização realizada com sucesso!","success");
        header("Location: /pessoa/perfil");

    } catch (Exception $e) {

        Model::setError($e->getMessage());
        Model::setError("Erro ao realizar atualização!","danger");
        header("Location: /pessoa/perfil/atualizacao");
    }

    exit();

});

$app->get('/admin/pessoa/consulta/:sCpf', function($sCpf) {

    User::verifyLogin();

    $oPessoaFisica = Pessoa::recuperaPessoaFisicaPorCpf($sCpf);

    echo json_encode($oPessoaFisica[0]);

});