<?php

namespace Hcode;

class Model {

    private $values = [];
    const ERROR = "Error";
    const ERROR_CLASS = "ErrorClass";

    public function __call($name, $args)
    {
        $method = substr($name, 0, 3);

        $fieldName = substr($name, 3, strlen($name));

        switch ($method)
        {
            case "get":
                return (isset($this->values[$fieldName])) ? $this->values[$fieldName] : NULL;

            case "set":
                $this->values[$fieldName] = $args[0];
                break;

        }
        return "Erro";
    }

    public function setData($data = array()){

        foreach ($data as $key => $value) {
            $key = str_replace("_"," ",$key);
            $key = ucwords($key);
            $key = str_replace(" ","",$key);
            $this->{"set".$key}($value);
        }

    }

    public function getValues(){

        return $this->values;

    }

    public static function setError($sMsg,$sClass = 'danger'){

        if (!isset($_SESSION))session_start();

        if (is_array($sMsg)){
            $_SESSION[Model::ERROR] = $sMsg[0];
            $_SESSION[Model::ERROR_CLASS] = $sMsg[1];
        } else{
            $_SESSION[Model::ERROR] = $sMsg;
            $_SESSION[Model::ERROR_CLASS] = $sClass;
        }
    }

    public static function getError(){

        if (!isset($_SESSION))session_start();

        $sMsg = (isset($_SESSION[Model::ERROR]) && $_SESSION[Model::ERROR]) ? $_SESSION[Model::ERROR] : '';

        $sClass = (isset($_SESSION[Model::ERROR_CLASS]) && $_SESSION[Model::ERROR_CLASS]) ? $_SESSION[Model::ERROR_CLASS] : '';

        Model::clearError();

        return ['sMsg'=>$sMsg,'sClass'=>$sClass];

    }

    public static function clearError(){

        $_SESSION[Model::ERROR] = NULL;
        $_SESSION[Model::ERROR_CLASS] = NULL;

    }

}