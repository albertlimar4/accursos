<?php

namespace Hcode\DB;

use PDO;
use PDOException;

class Sql {

    const HOSTNAME_ON = "162.241.33.176";
    const USERNAME_ON = "acads506_albert";
    const PASSWORD_ON = "F@dire304";
    const DBNAME_ON = "acads506_accursos";

    const HOSTNAME_LC = "127.0.0.1";
    const USERNAME_LC = "root";
    const PASSWORD_LC = "";
    const DBNAME_LC = "accursos";

    private $conn;

    public function __construct()
    {
        $sUrl = $_SERVER['HTTP_HOST'];

        if ($sUrl == "accursoseconsultoria.com" || $sUrl == "www.accursoseconsultoria.com"){

            error_reporting(0);

            $this->conn = new PDO(
                "mysql:dbname=".Sql::DBNAME_ON.";host=".Sql::HOSTNAME_ON.";charset=utf8",
                Sql::USERNAME_ON,
                Sql::PASSWORD_ON
            );

        } elseif ($sUrl == 'www.acadsoftaccursos.com.br' || $sUrl == "localhost"){

            $this->conn = new PDO(
                "mysql:dbname=".Sql::DBNAME_LC.";host=".Sql::HOSTNAME_LC.";charset=utf8",
                Sql::USERNAME_LC,
                Sql::PASSWORD_LC
            );

        } else{
            throw new PDOException("Erro na conexão");
        }

    }

    private function setParams($statement, $parameters = array())
    {

        foreach ($parameters as $key => $value) {

            $this->bindParam($statement, $key, $value);

        }

    }

    private function bindParam($statement, $key, $value)
    {

        $statement->bindParam($key, $value);

    }

    public function query($rawQuery, $params = array())
    {
//        echo $rawQuery."<hr>";
//        print_r($params);

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $params);

        $stmt->execute();

//        var_dump($stmt->errorInfo());die();

        return $stmt->errorInfo();

    }

    public function select($rawQuery, $vParam = array()):array
    {

//        echo "<hr>{$rawQuery}<hr>";
//        foreach ($vParam as $sParam) {
//            echo ($sParam) ? "'{$sParam}'," : "null,";
//        }

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $vParam);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

}