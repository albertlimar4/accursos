<?php

namespace Hcode;

class Upload
{
    private $arquivo_permissao;
    private $sDir;
    private $max_filesize;
    private $error = 0;
    private $file_type;
    private $file_size;
    private $nAltura;
    private $nLargura;
    private $temp;
    private $sDescricao;

    function __construct($permissao="" , $max_file = 12000000, $sDir = "arquivos", $sDescricao = null){
        if (empty ($permissao))
            $permissao = array ("text/plain","application/msword","application/vnd.ms-excel","application/vnd.ms-powerpoint","application/zip","application/pdf");
        $this->arquivo_permissao = $permissao;

        $this->max_filesize = $max_file;
        $this->sDir = $sDir;
        $this->sDescricao = $sDescricao;
    }

    function pegaExtensao($file) {
        return strrchr($file, '.');
    }

    function putFile($oFile,$nCodReferencia){

        $this->file_type = strtok($oFile['type'], ";");

        $this->file_size = $oFile['size'];
        $this->temp = $oFile['tmp_name'];  //upload para o diretorio temp

        $file_name = $nCodReferencia . $this->pegaExtensao($oFile['name']) ;

        $this->sDir;//$sDiretorio . "/";

        if (!in_array($this->file_type, $this->arquivo_permissao)) $this->Error(1);

        if (($this->file_size <= 0) || ($this->file_size > $this->max_filesize) ) $this->Error(2);

        if ($this->error == 0){
            $filename = basename($file_name);

            if(!is_dir($this->sDir))
                return [0=>"Diretório inexistente!",1=>"alert alert-danger"];

            if (!empty ($this->sDir))
                $sArquivo = $this->sDir . "/" . $filename;
            else
                $sArquivo = $filename;

            if (file_exists($sArquivo)){
                $this->Error(5);
            }

            if(!is_uploaded_file($this->temp)) $this->Error(3);

            if(!move_uploaded_file($this->temp, $sArquivo)){
                $this->Error(4);
            }else{
                return "/{$sArquivo}";
            }
            return $this->error;
        } else {
            return $this->error;
        }
    }

    function Error($op){
        switch ($op){
            case 0: return true;
            case 1: $this->error = "Erro 1: Este tipo de arquivo não tem permissão para upload: {$this->sDescricao} / {$this->file_type}."; break;
            case 2: $this->error = "Erro 2: Erro no tamanho do arquivo: {$this->sDescricao}. {$this->file_size}Kb. Tamanho Maximo é {$this->max_filesize}!"; break;
            case 3: $this->error = "Erro 3: Ocorreu um erro na transfêrencia do arquivo: {$this->sDescricao}."; break;
            case 4: $this->error = "Erro 4: Ocorreu um erro na transfêrencia de {$this->temp} para {$this->sDescricao}."; break;
            case 5: $this->error = "Erro 5: Já existe um arquivo com este nome, renomeie o arquivo {$this->sDescricao} e tente novamente! ";break;
            case 6: $this->error = "Erro 6: A altura do arquivo deve ser {$this->nAltura} px";break;
            case 7: $this->error = "Erro 7: A largura do arquivo deve ser {$this->nLargura} px";break;
        }

        return [0=>$this->error,1=>"alert alert-danger"];
    }
}