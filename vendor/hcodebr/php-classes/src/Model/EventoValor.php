<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;
use Exception;

/**
 * @method getValId()
 * @method getValDescricao()
 * @method getValValor()
 * @method getValInicio()
 * @method getValFim()
 * @method getEveId()
 * @method getValInicioFormatado()
 * @method getValFimFormatado()
 * @method setValInicioFormatado(string $maskData)
 * @method setValFimFormatado(string $maskData)
 */
class EventoValor extends Model {

    public static function valoresPorEvento($nEveId){

        $sql = new Sql();

        return $sql->select("Select * From ev_evento_valor Where eve_id = :nEveId Order By val_inicio", array(
            ":nEveId"=>$nEveId
        ));

    }

    public static function valoresPorEventoAtivo($nEveId){

        $sql = new Sql();

        return $sql->select("Select * From ev_evento_valor Where eve_id = :nEveId AND val_inicio <= DATE(NOW()) AND val_fim >= DATE(NOW()) Order By val_inicio", array(
            ":nEveId"=>$nEveId
        ));

    }

    public function getValor($nValId){

        $sql = new Sql();

        $voEventoValor = $sql->select("Select * From ev_evento_valor Where val_id = :nValId", array(
            ":nValId"=>$nValId
        ));

        if ($voEventoValor) {
            $this->setData($voEventoValor[0]);
            return true;
        }
        else
            return false;
    }

    public function update(){

        $sql = new Sql();

        $this->setValInicioFormatado(maskData($this->getValInicio(), true));
        $this->setValFimFormatado(maskData($this->getValFim(), true));

        $oEventoValor = $sql->select("CALL sp_EventoValorUpdate(:nValId, :sValDescricao, :nValValor, :dValInicio, :dValFim, :nEveId)", array(
            ":nValId"=>$this->getValId(),
            ":sValDescricao"=>$this->getValDescricao(),
            ":nValValor"=>formatDecimal($this->getValValor()),
            ":dValInicio"=>$this->getValInicioFormatado(),
            ":dValFim"=>$this->getValFimFormatado(),
            ":nEveId"=>$this->getEveId()
        ));

        $this->setData($oEventoValor[0]);

    }

    /**
     * @throws Exception
     */
    public function save(){

        $sql = new Sql();

        if ($this->getValDescricao()[0] != null){
            foreach ($this->getValDescricao() as $nKey=>$sValDescricao) {

                $this->setValInicioFormatado(maskData($this->getValInicio()[$nKey]));
                $this->setValFimFormatado(maskData($this->getValFim()[$nKey]));

                $oEventoValor = $sql->select("CALL sp_EventoValorSave(:sValDescricao, :nValValor, :dValInicio, :dValFim, :nEveId)", array(
                    ":sValDescricao"=>$this->getValDescricao()[$nKey],
                    ":nValValor"=>formatDecimal($this->getValValor()[$nKey]),
                    ":dValInicio"=>$this->getValInicioFormatado(),
                    ":dValFim"=>$this->getValFimFormatado(),
                    ":nEveId"=>$this->getEveId()
                ));

                if (!$oEventoValor)
                    return false;
                else
                    return true;
            }
        } else
            return false;

        return false;
    }

    public function deleteEventoValor($nValId){

        $sql = new Sql();

        $sql->query("Update ev_evento_valor SET val_ativo = 0 Where val_id = :nValId", array(
            ":nValId"=>$nValId
        ));

    }

}