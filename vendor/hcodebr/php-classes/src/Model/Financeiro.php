<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Financeiro extends Model {

    public function savePgto(){

        $sql = new Sql();

        $situacao = $sql->select("SELECT id FROM pagamento_situacao WHERE descricao = :descricao", array(
            ":descricao"=>"Pago"
        ));

        $sql->query("INSERT INTO ev_inscricao_pagamento (valor, obs, ev_evento_inscricao_id, pagamento_forma_id, pagamento_situacao_id) VALUES (:valor, :obs, :inscricao, :forma, :situacao)", array(
            ":valor"=>formatDecimal($this->getValor()),
            ":obs"=>$this->getObs(),
            ":inscricao"=>$this->getInscricao(),
            ":forma"=>$this->getForma(),
            ":situacao"=>$situacao[0]['id']
        ));

        $sql->query("UPDATE ev_evento_inscricao SET ev_inscricao_situacao_id = 1 WHERE evento_inscricao_id = :inscricao", array(
            ":inscricao"=>$this->getInscricao()
        ));

    }

}