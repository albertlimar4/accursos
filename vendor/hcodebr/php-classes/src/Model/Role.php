<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getId()
 * @method getDescricao()
 */
class Role extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM user_role WHERE ativo = 1 ORDER BY descricao");

    }

    public function save(){

        $sql = new Sql();

        $oRole = $sql->select("CALL sp_RoleSave(:sDescricao)", array(
            ":sDescricao"=>$this->getDescricao()
        ));

        $this->setData($oRole[0]);

    }

    public function get($RoleId){

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM user_role WHERE user_role_id = :nRoleId", array(
            ":nRoleId"=>$RoleId
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        $sql->query("CALL sp_RoleUpdate (:nRoleId, :sDescricao)", array(
            ":nRoleId"=>$this->getId(),
            ":sDescricao"=>$this->getDescricao()
        ));
    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM user_role WHERE user_role_id = :nRoleId", array(
            ":nRoleId"=>$this->getId()
        ));

    }

}