<?php

namespace Hcode\Model;

use Exception;
use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getEveId()
 * @method getPfiId()
 * @method setSitId($nSitId)
 * @method getSitId()
 * @method getCatId()
 * @method getEviArea()
 * @method getPesId()
 * @method getPfiUfNatural()
 * @method getEviId()
 * @method getEveNome()
 */
class Inscricao extends Model {

    public static function listAll($nEviAtivo = 1){

        $sql = new Sql();

        return $sql->select("Select * From v_inscricoes Where EviAtivo = :nEviAtivo Order By PfiNome", [
            ":nEviAtivo"=>$nEviAtivo
        ]);

    }

    public function consultaCpf($sPfiCpf){

        $sql = new Sql();

        $oInscricao = $sql->select("Select * From v_inscricoes Where PfiCpf = :sPfiCpf And EveId = :nEveId", [
            ":sPfiCpf"=>$sPfiCpf,
            ":nEveId"=>$this->getEveId()
        ]);

        return ($oInscricao) ? true : false;

    }

    public function inscricaoPorPessoaEvento($nPfiId, $nEveId){

        $sql = new Sql();

        $oInscricao = $sql->select("Select * From v_inscricoes Where PfiId = :nPfiId And EveId = :nEveId", [
            ":nPfiId"=>$nPfiId,
            ":nEveId"=>$nEveId
        ]);

        return ($oInscricao) ? true : false;

    }

    /**
     * @return array
     * @throws Exception
     */
    public function save(){

        $oPessoa = new Pessoa();
        $oPessoa->setData($this->getValues());
        $oPessoa->savePessoaFisica();

        $oUser = new User();
        $oUser->setData($oPessoa->getValues());
        $oUser->save();

        $this->setData($oUser->getValues());

        $oInscricao = self::consultaCpf($oPessoa->getPfiCpf());

        $oEndereco = new Endereco();
        $oEndereco->setData($oPessoa->getValues());
        $oEndereco->update();
        $this->setData($oEndereco->getValues());

        $oTelefone = new Telefone();
        $oTelefone->setData($oPessoa->getValues());
        $oTelefone->save();
        $this->setData($oTelefone->getValues());

        if ($oInscricao)
            throw new Exception("Inscrição já realizada anteriormente.");
        else
            $this->saveInscricao();

        return $this->getValues();

    }

    public function update(){

        $this->alteraSituacao();

        $oPessoa = new Pessoa();
        $oPessoa->setData($this->getValues());
        $oPessoa->UpdatePessoaFisica();

        $oUser = new User();
        $oUser->setData($oPessoa->getValues());
        $oUser->update();

        $oTelefone = new Telefone();
        $oTelefone->setData($oPessoa->getValues());
        $oTelefone->update();

        return true;

    }

    /**
     * @throws Exception
     */
    public function saveInscricao(){

        $sql = new Sql();

        $oInscricao = self::inscricaoPorPessoaEvento($this->getPfiId(), $this->getEveId());

        if ($oInscricao)
            return [0=>"Inscrição já realizada anteriormente.",1=>"alert alert-danger"];
        else{

            $oSituacao = $sql->select("Select sit_id From ev_inscricao_situacao Where sit_descricao = :sSitDescricao", array(
                ":sSitDescricao"=>"Pendente"
            ));

            $this->setSitId($oSituacao[0]['sit_id']);

            $oInscricao = $sql->select("CALL sp_InscricaoSave(:nEveId, :nSitId, :sEviCategoria, :sEviArea, :nPfiId)", array(
                ':nEveId'=>$this->getEveId(),
                ':nSitId'=>$this->getSitId(),
                ':sEviCategoria'=>$this->getCatId(),
                ':sEviArea'=>$this->getEviArea(),
                ":nPfiId"=>$this->getPfiId()
            ));

            if ($oInscricao)
                $this->setData($oInscricao[0]);
            else
                throw new Exception("Erro ao cadastrar inscrição.");
        }
    }

    /**
     * @param $nEviId
     * @throws Exception
     */
    public function get($nEviId){

        $sql = new Sql();

        $oEventoInscricao = $sql->select("Select * From v_inscricoes Where EviId = :nEviId", [
            ":nEviId"=>$nEviId
        ]);

        if ($oEventoInscricao)
            $this->setData($oEventoInscricao[0]);
        else
            throw new Exception("Nenhum registro encontrado.");

    }

    public function deleteInscricao($nEviId){

        $sql = new Sql();

        $vResult = $sql->query("Delete From ev_evento_inscricao Where evi_id = :nEviId", array(
            ":nEviId"=>$nEviId
        ));

        if ($vResult[0] == 0)
            return ["Inscrição excluída com sucesso","success"];
        else
            return ["Erro ao excluir a Inscrição. Código: {$vResult[0]}","danger"];

    }

    public function alteraSituacao(){

        $sql = new Sql();

        $sql->query("Update ev_evento_inscricao Set sit_id = :nSitId Where evi_id = :nEviId", [
            ":nEviId"=>$this->getEviId(),
            ":nSitId"=>$this->getSitId()
        ]);

    }


}