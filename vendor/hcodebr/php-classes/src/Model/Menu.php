<?php


namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getMenId()
 * @method getMenDescricao()
 * @method getMenLink()
 * @method getMenIcone()
 * @method getUroId()
 * @method getMenuPai()
 */
class Menu extends Model
{

    public static function menuListAll() {

        $sql = new Sql();

        return $sql->select("Select * From menu Where men_ativo = 1 Order By men_ordem");

    }

    public static function listaMenuPai($nUroId) {

        $sql = new Sql();

        return $sql->select("Select men.men_id as MenId, men.men_descricao, men.men_icone, men.men_link, mro.uro_id From menu men Inner Join menu_role mro On mro.men_id = men.men_id Where mro.uro_id = :nUroId And men_ativo = 1 And menu_pai Is Null Group By men.men_id, men_ordem Order By men_ordem", [
            ":nUroId"=>$nUroId
        ]);

    }

    public static function listaMenuFilho($nMenuPai, $nUroId) {

        $sql = new Sql();

        return $sql->select("Select men.men_id as MenuId, men.men_descricao, men.men_icone, men.men_link, mro.uro_id From menu men Inner Join menu_role mro On mro.men_id = men.men_id Where men.menu_pai = :nMenuPai And mro.uro_id = :nUroId And men.men_ativo = 1 Group By men.men_id, men.men_ordem ORDER BY men.men_ordem", [
            ":nUroId"=>$nUroId,
            ":nMenuPai"=>$nMenuPai
        ]);

    }

    public static function montaMenu($nUroId){

        $voMenuPai = self::listaMenuPai($nUroId);

        foreach ($voMenuPai as $nKey=>$oMenuPai) {

            $voMenuFilho = self::listaMenuFilho($oMenuPai['MenId'],$nUroId);

            if ($voMenuFilho) {
                $voMenuPai[$nKey]['sub_menu'] = $voMenuFilho;
            }

        }

        return $voMenuPai;

    }

    public function menuSave(){

        $sql = new Sql();

        $nMenuPai = ($this->getMenuPai()) ? $this->getMenuPai() : null;

        $oMenu = $sql->select("CALL sp_MenuSave(:sMenDescricao, :sMenLink, :sMenIcone, :nMenuPai)", array(
            ":sMenDescricao"=>$this->getMenDescricao(),
            ":sMenLink"=>$this->getMenLink(),
            ":sMenIcone"=>$this->getMenIcone(),
            ":nMenuPai"=>$nMenuPai
        ));

        $this->setData($oMenu[0]);

    }

    public function get($nMenId){

        $sql = new Sql();

        $oMenu = $sql->select("Select * From menu Where men_id = :nMenId", array(
            ":nMenId"=>$nMenId
        ));

        $this->setData($oMenu[0]);

    }

    public static function menuOrdem($nOrdem, $nMenuId){

        $sql = new Sql();

        $sql->query("Update menu Set men_ordem = :nOrdem Where men_id = :nMenuId", [
            ":nOrdem"=>$nOrdem,
            ":nMenuId"=>$nMenuId
        ]);

    }

    public function menuUpdate(){

        $nMenuPai = ($this->getMenuPai()) ? $this->getMenuPai() : null;

        $sql = new Sql();

        $oMenu = $sql->select("CALL sp_MenuUpdate(:nMenId, :sMenDescricao, :sMenLink, :sMenIcone, :nMenuPai)", array(
            ":nMenId"=>$this->getMenId(),
            ":sMenDescricao"=>$this->getMenDescricao(),
            ":sMenLink"=>$this->getMenLink(),
            ":sMenIcone"=>$this->getMenIcone(),
            ":nMenuPai"=>$nMenuPai
        ));

        return $oMenu[0];
    }

    public function delete(){

        $sql = new Sql();

        $sql->query("Delete From menu Where men_id = :nMenId", array(
            ":nMenId"=>$this->getMenId()
        ));

    }

    public function saveMenuRole(){

        $sql = new Sql();

        $sql->query("Delete From menu_role Where uro_id = :nUroId", [
            ":nUroId"=>$this->getUroId()
        ]);

        $sql->query("Alter Table menu_role Auto_Increment = 1");

        foreach ($this->getMenId() as $nMenuId) {

            $sql->select("CALL sp_MenuRoleSave(:nMenId, :nUroId)", [
                ":nMenId"=>$nMenuId,
                ":nUroId"=>$this->getUroId()
            ]);

        }

    }

}