<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Log extends Model {

    public static function listAttempts(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM user_acesso a INNER JOIN `user` b ON a.user_id = b.id INNER JOIN pessoa c ON c.id = b.pessoa_id INNER JOIN pessoa_fisica d ON c.id = d.pessoa_id INNER JOIN ead_aluno e ON e.pessoa_fisica_id = d.id ORDER BY data DESC LIMIT 100");

    }

    public static function acessosHoje(){

        $sql = new Sql();

        $dia = date('d');

        return $sql->select("SELECT DISTINCT user_id FROM user_acesso a INNER JOIN `user` b ON a.user_id = b.id WHERE DATE(data) = DATE(NOW()) ORDER BY data DESC");

    }

    public static function acessosUnicosMes(){

        $sql = new Sql();

        $mes = date('m');

        return $sql->select("SELECT DISTINCT user_id as id, e.nome as nome FROM user_acesso a INNER JOIN `user` b ON a.user_id = b.id INNER JOIN pessoa d ON d.id = b.pessoa_id INNER JOIN pessoa_fisica e ON e.pessoa_id = d.id INNER JOIN ead_aluno c ON c.pessoa_fisica_id = e.id WHERE MONTH(data) = :mes AND b.user_role_id = 2 ORDER BY e.nome", array(
            ":mes"=>$mes
        ));

    }

    public static function acessosSemana(){
        $sql = new Sql();

        $sql->select("SET lc_time_names = 'pt_PT'");

        return $sql->select("SELECT DATE_FORMAT(`data`,'%a (%d/%m)') as dia, COUNT(DAY(`data`)) as acessos FROM user_acesso WHERE data BETWEEN CURDATE() - INTERVAL 6 DAY AND CURDATE() + 1 GROUP BY DAY(data)");

    }

}