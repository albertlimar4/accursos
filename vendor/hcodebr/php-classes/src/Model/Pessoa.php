<?php

namespace Hcode\Model;

use Exception;
use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getPfiId()
 * @method getProfissao()
 * @method getPfiNome()
 * @method getPfiCpf()
 * @method getPfiNascimento()
 * @method getPesEmail()
 * @method getPfiSexo()
 * @method getPfiNaturalidade()
 * @method getPfiUfNatural()
 * @method getPrsDescricao()
 * @method getEscDescricao()
 * @method getEciDescricao()
 * @method getPesId()
 * @method getUseEmail()
 *
 */

class Pessoa extends Model {

    public static function recuperaPessoaFisicaPorCpf($sPfiCpf){

        $sql = new Sql();

        $oPessoaFisica = $sql->select("SELECT * FROM pessoa_fisica WHERE pfi_cpf = :sPfiCpf", array(
            ":sPfiCpf"=>$sPfiCpf
        ));

        return ($oPessoaFisica) ? $oPessoaFisica : false;

    }

    public function savePessoaFisica(){

        $sPfiNome = trim(mb_strtoupper($this->getPfiNome(), 'utf-8'));
        $sPfiCpf = removeAcentos(mask_inv_cpf(removeEspacos($this->getPfiCpf())));

        $sql = new Sql();

        $oPessoaFisica = self::recuperaPessoaFisicaPorCpf($sPfiCpf);

        if (!$oPessoaFisica){
            $oPessoaFisica = $sql->select("CALL sp_PessoaFisicaSave (:sPfiNome, :sPesEmail, :sPfiSexo, :sPfiCpf)", [
                ":sPfiNome"=>$sPfiNome,
                ":sPesEmail"=>$this->getPesEmail(),
                ":sPfiSexo"=>$this->getPfiSexo(),
                ":sPfiCpf"=>$sPfiCpf,
            ]);
        }

        if ($oPessoaFisica) {
            $this->setData($oPessoaFisica[0]);
            return true;
        } else
            return false;

    }

    public function UpdatePessoaFisica(){

        $sql = new Sql();

        $sPfiNome = trim(mb_strtoupper($this->getPfiNome(), 'utf-8'));
        $sPfiCpf = removeAcentos(mask_inv_cpf(removeEspacos($this->getPfiCpf())));

        $oPessoaFisica = $sql->select("CALL sp_PessoaFisicaUpdate (:nPfiId, :nPesId, :sPfiNome, :sPesEmail, :sPfiSexo, :dPfiNascimento, :sPfiCpf, :sEciDescricao, :sPfiNaturalidade, :sPfiUfNatural, :sPrsDescricao, :sEscDescricao)", [
            ":nPfiId"=>$this->getPfiId(),
            ":nPesId"=>$this->getPesId(),
            ":sPfiNome"=>$sPfiNome,
            ":sPesEmail"=>$this->getUseEmail(),
            ":sPfiSexo"=>$this->getPfiSexo(),
            ":dPfiNascimento"=>($this->getPfiNascimento()) ? maskData($this->getPfiNascimento()) : null,
            ":sPfiCpf"=>$sPfiCpf,
            ":sEciDescricao"=>$this->getEciDescricao(),
            ":sPfiNaturalidade"=>$this->getPfiNaturalidade(),
            ":sPfiUfNatural"=>$this->getPfiUfNatural(),
            ":sPrsDescricao"=>$this->getPrsDescricao(),
            ":sEscDescricao"=>$this->getEscDescricao()
        ]);

        $this->setData($oPessoaFisica[0]);

    }

    public function update(){

        $oPessoa = new Pessoa();
        $oPessoa->setData($this->getValues());
        $oPessoa->UpdatePessoaFisica();

        $oUser = new User();
        $oUser->setData($oPessoa->getValues());
        $oUser->update();

        $oEndereco = new Endereco();
        $oEndereco->setData($oPessoa->getValues());
        $oEndereco->update();

        $oTelefone = new Telefone();
        $oTelefone->setData($oPessoa->getValues());
        $oTelefone->update();

        return true;

    }

    public static function fotoPessoa($nUseId){

        $sql = new Sql();

        $oFoto = $sql->select("SELECT FotId,FotUrl FROM v_usuarios WHERE UseId = :nUseId", array(
            ":nUseId"=>$nUseId
        ));

        if ($oFoto){
            return $oFoto[0];
        }

        return false;

    }

    /**
     * @param $oFile
     * @param $nPesId
     * @param $nFotId
     * @return array|bool
     * @throws Exception
     */
    public function updatePerfilFoto($oFile, $nPesId, $nFotId){

        if ($oFile['error']){
            echo json_encode(['msg'=>$oFile['error'],'sucesso'=>false]);
            die();
        }

        $dirUploads = "uploads".DIRECTORY_SEPARATOR."users";

        if (!is_dir($dirUploads)){
            mkdir($dirUploads);
        }

        if (move_uploaded_file($oFile['tmp_name'], $dirUploads.DIRECTORY_SEPARATOR.$nPesId.".png")){
            $sFotUrl = DIRECTORY_SEPARATOR.$dirUploads.DIRECTORY_SEPARATOR.$nPesId.".png";
        } else{
            echo json_encode(['msg'=>"Não foi possível salvar a foto!",'sucesso'=>false]);
            die();
        }

        $sql = new Sql();

        if ($nFotId){
            $sql->query("Update aux_foto SET fot_url = :sFotUrl WHERE fot_id = :nFotId", array(
                ":sFotUrl"=>$sFotUrl,
                ":nFotId"=>$nFotId
            ));
            echo json_encode(['msg'=>'Foto alterada com sucesso','url'=>$sFotUrl,'sucesso'=>true]);
        } else {
            $oFoto = $sql->select("CALL sp_PessoaFotoSave (:sFotUrl, :nPesId)", array(
                ":sFotUrl"=>$sFotUrl,
                ":nPesId"=>$nPesId
            ));

            if ($oFoto)
                echo json_encode(['msg'=>'Foto alterada com sucesso','url'=>$sFotUrl,'sucesso'=>true]);
            else
                echo json_encode(['msg'=>"Erro ao inserir imagem",'sucesso'=>false]);
        }

        return false;

    }

}