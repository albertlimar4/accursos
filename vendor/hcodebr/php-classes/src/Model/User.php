<?php

namespace Hcode\Model;

use Exception;
use Hcode\DB\Sql;
use Hcode\Mailer;
use Hcode\Model;

/**
 * @method getUseId()
 * @method getUseSenha()
 * @method getUseAtivo()
 * @method getPrimeiroAcesso()
 * @method getUroId()
 * @method getUroDescricao()
 * @method getUseAtualizarSenha()
 * @method getUseEmail()
 * @method getPfiNome()
 * @method getPfiCpf()
 * @method getPesId()
 * @method getTotal()
 * @method getPfiUfNatural()
 */

class User extends Model {

    const SESSION = "User";
    const SECRET = "HcodePhp7_Secret";
    const ERROR_REGISTER = "UserErrorRegister";

    /**
     * @param $sUseLogin
     * @param $sUseSenha
     * @throws Exception
     */
    public static function login($sUseLogin, $sUseSenha){

        $sql = new Sql();

        $oUsuario = $sql->select("Select * From v_usuarios Where UseLogin = :sUseLogin Or UseEmail = :sUseLogin", array(
            ":sUseLogin"=>$sUseLogin
        ));

        if (!$oUsuario)
            throw new Exception("Usuário não cadastrado!");

        $oUser = new User();

        $oUser->setData($oUsuario[0]);

        if ($oUser->getUseAtivo() == 0)
            throw new Exception("Usuário desativado!");

        if (password_verify($sUseSenha, $oUser->getUseSenha()) === true){
            if (!isset($_SESSION))session_start();

            $_SESSION[User::SESSION] = $oUser->getValues();

            $_SESSION[User::SESSION]['UltimaAtividade'] = time();

            $sql->query("Insert Into user_acesso (uac_ip, uac_so, uac_browser, use_id) Values (:sUacIp, :sUacSo, :sUacBrowser, :nUseId)", array(
                ":sUacIp"=>$_SERVER['REMOTE_ADDR'],
                ":sUacSo"=>getOS(),
                ":sUacBrowser"=>getBrowser(),
                ":nUseId"=>$oUser->getUseId()
            ));

            if ($oUser->getUseAtualizarSenha() == 1){
                header("Location: /admin/password");
                exit();
            }

            switch ($oUser->getUroDescricao()){
                case "Desenvolvedor":
                case "Administrador":
                    header("Location: /admin");
                    exit();
                case "Avaliador":
                    header("Location: /avaliacao");
                    exit();
                case "Participante":
                    header("Location: /aluno");
                    exit();
                default:
                    throw new Exception("Usuário inexistente ou senha inválida");
            }

        } else {

            throw new Exception("Senha inválida!");
        }

    }

    public static function verifyLogin(){

        if (!isset($_SESSION))session_start();

        if (
            !isset($_SESSION[User::SESSION])
            ||
            !$_SESSION[User::SESSION]
            ||
            !(int)$_SESSION[User::SESSION]["UseId"] > 0
            ||
            $_SESSION[User::SESSION]["UseAtivo"] != 1
            ||
            (time() - $_SESSION[User::SESSION]['UltimaAtividade'] > (2*3600))

        ) {

            header("Location: /admin/login");
            exit();

        } else {

            $_SESSION[User::SESSION]['UltimaAtividade'] = time();

            if (
                (int)$_SESSION[User::SESSION]["UroId"] == 1
                ||
                (int)$_SESSION[User::SESSION]["UroId"] == 2
            ){

                return true;

            } else{

                header("Location: /admin/login");
                exit();

            }

        }

    }

    public static function verifyLoginaluno(){

        if (!isset($_SESSION))session_start([
            'cookie_lifetime' => 3*3600,
        ]);

        if (
            !isset($_SESSION[User::SESSION])
            ||
            !$_SESSION[User::SESSION]
            ||
            !(int)$_SESSION[User::SESSION]["UsoId"] > 0
            ||
            $_SESSION[User::SESSION]["UsoAtivo"] != 1

        ) {

            header("Location: /admin/login");
            exit();

        }

    }

    public static function logout(){

        if (!isset($_SESSION))session_start();

        $_SESSION[User::SESSION] = null;

        session_destroy();
    }

    public static function listAll($nUseAtivo = 1){

        $sql = new Sql();

        return $sql->select("Select * From v_usuarios WHERE UseAtivo = :nUsoAtivo Order By PfiNome", [
            ":nUseAtivo"=>$nUseAtivo
        ]);

    }

    public function recuperaUsuarioPorLogin($sUseLogin){

        $sql = new Sql();

        $oUser = $sql->select("Select * From v_usuarios WHERE UseLogin = :sUseLogin Order By PfiNome", [
            ":sUseLogin"=>$sUseLogin
        ]);

        return ($oUser) ? $oUser : false;

    }

    public function save($nUroId = 4){

        $sUseSenha = password_hash($this->getPfiCpf(), PASSWORD_DEFAULT, [
            "cost"=>12
        ]);

        $sUseLogin = removeAcentos(mask_inv_cpf(removeEspacos($this->getPfiCpf())));
        $sUseEmail = trim(strtolower($this->getUseEmail()));
        $nUroId = ($this->getUroId()) ? $this->getUroId() : $nUroId;

        $oUser = $this->recuperaUsuarioPorLogin($sUseLogin);

        if (!$oUser){
            $sql = new Sql();

            $oUser = $sql->select("CALL sp_UserSave(:sUseLogin, :sUseEmail, :sUseSenha, :nPesId, :nUroId)", array(
                ":sUseLogin"=>$sUseLogin,
                ":sUseEmail"=>$sUseEmail,
                ":sUseSenha"=>$sUseSenha,
                ":nPesId"=>$this->getPesId(),
                ":nUroId"=>$nUroId
            ));
        }

        $this->setData($oUser[0]);
    }

    public function get($nUseId){

        $sql = new Sql();

        $oUser = $sql->select("Select * From v_usuarios Where UseId = :nUseId", array(
            ":nUseId"=>$nUseId
        ));

        $this->setData($oUser[0]);

    }

    public function update(){

        $sPfiNome = trim(mb_strtoupper($this->getPfiNome()), 'utf-8');
        $sPfiCpf = removeAcentos(mask_inv_cpf(removeEspacos($this->getPfiCpf())));
        $sUseEmail = trim(strtolower($this->getUseEmail()));

        $sql = new Sql();

        $oUser = $sql->select("CALL sp_UserUpdate(:nPesId, :nUseId, :sPfiNome, :sUseLogin, :sUseEmail, :nUroId)", array(
            ":nPesId"=>$this->getPesId(),
            ":nUseId"=>$this->getUseId(),
            ":sPfiNome"=>$sPfiNome,
            ":sUseLogin"=>$sPfiCpf,
            ":sUseEmail"=>$sUseEmail,
            ":nUroId"=>$this->getUroId()
        ));

        $this->setData($oUser[0]);

    }

    /**
     * @param $sUseEmail
     * @param $sUseLogin
     * @param int $nUseAtivo
     * @return mixed
     * @throws Exception
     */
    public static function getForgot($sUseEmail, $sUseLogin, $nUseAtivo = 1){

        $sql = new Sql();

        $oResults = $sql->select("Select * From v_usuarios Where UseEmail = :sUseEmail And UseLogin = :sUseLogin AND UseAtivo = :nUseAtivo", array(
            ":sUseEmail"=>$sUseEmail,
            ":sUseLogin"=>$sUseLogin,
            ":nUseAtivo"=>$nUseAtivo
        ));

        if (!$oResults){
            throw new Exception("Usuário não encontrado!");
        } else{

            $oUser = new User();

            $oUser->setData($oResults[0]);

            $oPasswordRecovery = $sql->select("CALL sp_UserPasswordRecoverySave(:nUseId, :sUacIp, :sUacSo, :sUacBrowser)", [
                ":nUseId"=>$oUser->getUseId(),
                ":sUacIp"=>$_SERVER['REMOTE_ADDR'],
                ":sUacSo"=>getOS(),
                ":sUacBrowser"=>getBrowser(),
            ]);

            if (!$oPasswordRecovery){
                throw new Exception("Não foi possível recuperar a senha!");
            } else{

                $oDataRecovery = $oPasswordRecovery[0];

                $sIv = random_bytes(openssl_cipher_iv_length('aes-256-cbc'));

                $sCode = openssl_encrypt($oDataRecovery['ure_id'], 'aes-256-cbc', User::SECRET, 0, $sIv);

                $sResult = base64_encode($sIv.$sCode);

                $sUrl = $_SERVER['HTTP_HOST'];
                $sLink = "https://$sUrl/admin/forgot/reset?sCode=$sResult";

                $sAssunto = "Redefinir Senha";

                $oMailer = new Mailer($oUser->getUseEmail(), mask_nome($oUser->getPfiNome()), $sAssunto, "forgot", array(
                    "sPfiNome"=>mask_nome($oUser->getPfiNome()),
                    "sLink"=>$sLink
                ));

                $oMailer->send();

                return $oResults[0];

            }

        }

    }

    /**
     * @param $oResult
     * @return mixed
     * @throws Exception
     */
    public static function validforgotDecrypt($oResult){

        $oResult = base64_decode($oResult);

        $sCode = mb_substr($oResult, openssl_cipher_iv_length('aes-256-cbc'), null, '8bit');

        $sIv = mb_substr($oResult, 0, openssl_cipher_iv_length('aes-256-cbc'), '8bit');

        $nUreId = openssl_decrypt($sCode, 'aes-256-cbc', User::SECRET, 0, $sIv);

        $sql = new Sql();

        $oUserRecovery = $sql->select("Select `use`.UseId, ure.ure_id as UreId, `use`.PfiNome From user_recovery ure Inner Join user_acesso uac ON ure.uac_id = uac.uac_id Inner Join v_usuarios `use` On uac.use_id = `use`.UseId Where ure.ure_id = :nUreId And ure.ure_dtrecovery Is Null And DATE_ADD(ure.ure_dtregister, Interval 1 Hour) >= NOW()", array(
            ":nUreId"=>$nUreId
        ));

        if (!$oUserRecovery){
            throw new Exception("Não foi possível recuperar a senha: código inválido.");
        }
        else{
            return $oUserRecovery[0];
        }

    }

    public static function setForgotUsed($nUreId){

        $sql = new Sql();

        $sql->query("Update user_recovery SET ure_dtrecovery = NOW() WHERE ure_id = :nUreId", array(
            ":nUreId"=>$nUreId
        ));

    }

    public function updatePassword($sUseSenha, $nUseId){

        $sql = new Sql();

        $sql->query("Update user Set use_senha = :sUseSenha, use_atualizar_senha = 0 WHERE use_id = :nUseId", array(
            ":sUseSenha"=>$sUseSenha,
            ":nUseId"=>$nUseId
        ));

    }

    public function lockUnlock($nUseAtivo){

        $sql = new Sql();

        $sql->query("Update user Set use_ativo = :nUsoAtivo WHERE use_id = :nUseId", array(
            ":nUseAtivo"=>$nUseAtivo,
            ":nUseId"=>$this->getUseId()
        ));

    }

}