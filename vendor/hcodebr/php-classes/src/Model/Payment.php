<?php

namespace Hcode\Model;

use Cassandra\Date;
use Cassandra\Decimal;
use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getCreditCardToken()
 * @method getInstallmentQuantity()
 * @method getInstallmentValue()
 * @method getCreditCardHolderName()
 * @method getCreditCardHolderCPF()
 * @method getCreditCardHolderBirthDate()
 * @method getCreditCardHolderAreaCode()
 * @method getCreditCardHolderPhone()
 * @method getBillingAddressCountry()
 * @method setBillingAddressCountry(string $sCountry = 'BRL')
 * @method getEndRua()
 * @method getEndNumero()
 * @method getEndComplemento()
 * @method getEndBairro()
 * @method getEndCep()
 * @method getEndCidade()
 * @method getEndEstado()
 * @method getSenderName()
 * @method getSenderCpf()
 * @method getSenderAreaCode()
 * @method getSenderPhone()
 * @method getSenderEmail()
 * @method getSenderHash()
 * @method getShippingAddressRequired()
 * @method getShippingAddressStreet()
 * @method getShippingAddressNumber()
 * @method getShippingAddressComplement()
 * @method getShippingAddressDistrict()
 * @method getShippingAddressPostalCode()
 * @method getShippingAddressCity()
 * @method getShippingAddressState()
 * @method getShippingAddressCountry()
 * @method getShippingType()
 * @method getShippingCost()
 * @method getItemId()
 * @method getItemDescription()
 * @method getItemAmount()
 * @method getItemQuantity()
 * @method getPaymentMethod()
 * @method getEviId()
 */
class Payment extends Model{

    /**
     * Parâmetros de Autenticação
     */
    private $sService;
    private $sScript;
    private $sEmail;
    private $sToken;
    private $bSandbox = false;
    private $sMoeda;
    private $sNotificacao;
    private $sPaymentMode;

    /**
     * Parâmetros do REST
     */
    private $sAction;
    private $sCallback;
    private $voParams;
    private $voCardParams;

    /**
     * Atributo de sessão da conexão
     */
    private $sSessionId;

    /**
     * <b>Construtor</b>: Não é necessário fazer instância desse método.
     * Responsabilidade de setar os parâmetros de autenticação com o WebService
     */
    public function __construct()
    {
        $this->sEmail = 'albertlimar4@gmail.com'; // E-mail de Login no PagSeguro
        $this->sMoeda = 'BRL';
        $this->sNotificacao = 'https://accursos.acadsoft.com.br/pagseguro/notificacao';
        $this->sPaymentMode = 'default';

        if ($this->bSandbox){
            $this->sService = 'https://ws.sandbox.pagseguro.uol.com.br/v2/';
            $this->sToken = 'A176729820354053B620891C8980188B'; // Token da conta no PagSeguro
            $this->sScript = 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
        } else{
            $this->sService = 'https://ws.pagseguro.uol.com.br/v2/';
            $this->sToken = '4d09cfb6-c84d-419d-9350-5209572a47ebca56ea9c4c73b440f393753bc9732f48e1cf-b8e0-4898-a4f9-995307fb5b76'; // Token da no PagSeguro
//            $this->sToken = 'a12ad5bc-dc1e-43ac-9806-62fb56b2aeafed78d0ef4fb3abcecbf39925e91bf148a53a-c3ae-4c14-9114-18f5cae0e38f'; // Token da no PagSeguro
            $this->sScript = 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
        }
    }

    /**
     * <b>createPlan</b>: Método responsável por fazer a criação de um novo plano
     * no PagSeguro utilizando o WebService
     * @param INT $ref = ID do plano no banco de dados
     * @param STRING $name = Nome do plano
     * @param STRING $period = Periodicidade de cobrança [WEEKLY, MONTHLY, BIMONTHLY, TRIMONTHLY, SEMIANNUALLY ou YEARLY]
     * @param DECIMAL $amount = Valor do plano com duas casas decimais separadas por ponto (100.00)
     */
    public function createPlan($ref, $name, $period, $amount)
    {
        $this->sAction = '/pre-approvals/request';
        $this->voParams = [
            'reference' => $ref,
            'preApproval' => [
                'name' => $name,
                'charge' => 'AUTO',
                'period' => $period,
                'amountPerPayment' => $amount,
            ],
        ];
        $this->post();
    }

    /**
     * <b>createPlan</b>: Método responsável por fazer a criação de um novo plano
     * no PagSeguro utilizando o WebService
     */
    public function createTransaction(){

        $this->sAction = "transactions";

        $this->voCardParams = [
            'creditCardToken' => $this->getCreditCardToken(),
            'installmentQuantity' => $this->getInstallmentQuantity(),
            'installmentValue' => $this->getInstallmentValue(),
            'noInterestInstallmentQuantity' => 3,
            'creditCardHolderName' => $this->getCreditCardHolderName(),
            'creditCardHolderCPF' => mask_inv_cpf($this->getCreditCardHolderCPF()),
            'creditCardHolderBirthDate' => $this->getCreditCardHolderBirthDate(),
            'creditCardHolderAreaCode' => $this->getCreditCardHolderAreaCode(),
            'creditCardHolderPhone' => $this->getCreditCardHolderPhone(),
            'billingAddressStreet' => $this->getEndRua(),
            'billingAddressNumber' => $this->getEndNumero(),
            'billingAddressComplement' => $this->getEndComplemento(),
            'billingAddressDistrict' => $this->getEndBairro(),
            'billingAddressPostalCode' => $this->getEndCep(),
            'billingAddressCity' => $this->getEndCidade(),
            'billingAddressState' => $this->getEndEstado(),
            'billingAddressCountry' => 'BRL'
        ];

        $this->voParams = [
            'paymentMode' => "default",
            'paymentMethod' => $this->getPaymentMethod(),
            'receiverEmail' => $this->sEmail,
            'currency' => $this->sMoeda,
            'extraAmount' => "0.00",
            'notificationURL' => $this->sNotificacao,
            'reference' => $this->sAction,
            'senderName' => $this->getSenderName(),
            'senderCPF' => $this->getSenderCpf(),
            'senderAreaCode' => $this->getSenderAreaCode(),
            'senderPhone' => $this->getSenderPhone(),
            'senderEmail' => $this->getSenderEmail(),
//            'senderEmail' => "c11824183990223720115@sandbox.pagseguro.com.br",
            'senderHash' => $this->getSenderHash(),
            'shippingAddressStreet' => $this->getShippingAddressStreet(),
            'shippingAddressNumber' => $this->getShippingAddressNumber(),
            'shippingAddressComplement' => $this->getShippingAddressComplement(),
            'shippingAddressDistrict' => $this->getShippingAddressDistrict(),
            'shippingAddressPostalCode' => $this->getShippingAddressPostalCode(),
            'shippingAddressCity' => $this->getShippingAddressCity(),
            'shippingAddressState' => $this->getShippingAddressState(),
            'shippingAddressCountry' => $this->getShippingAddressCountry(),
            'shippingType' => $this->getShippingType(),
            'shippingCost' => "0.00",
            'itemId1' => $this->getItemId(),
            'itemDescription1' => $this->getItemDescription(),
            'itemAmount1' => $this->getItemAmount(),
            'itemQuantity1' => 1,
            'itemWeight1' => 0
        ];

        if ($this->getPaymentMethod() == 'creditCard')
            $this->voParams = array_merge($this->voParams,$this->voCardParams);

        //echo json_encode($this->voParams);die();
        $this->post();

    }

    /**
     * <b>createMemberShip</b>: Método responsável por fazer a adesão de um
     * cliente ao plano.
     * @param STRING $plan = Código do plano junto ao PagSeguro
     * @param STRING $ref = ID da assinatura no banco de dados
     * @param STRING $name = Nome do Cliente
     * @param STRING $email = E-mail do Cliente
     * @param STRING $document = CPF do Cliente
     * @param STRING $phone = Telefone de contato junto com o DDD (Sem pontuação)
     * @param STRING $street = Endereço do Cliente
     * @param STRING $number = Número do Endereço
     * @param STRING $complement = Complemento do Endereço
     * @param STRING $district = Bairro do Endereço
     * @param STRING $city = Cidade do Endereço
     * @param STRING $state = Estado do Endereço
     * @param STRING $country = Sigla do País com 3 letras (BRA)
     * @param STRING $postalCode = CEP do Endereço
     * @param STRING $token = Token do cartão de crédito
     * @param STRING $holderName = Nome do Titular do Cartão
     * @param DATE $holderBirth = Data de Nascimento do Titular do Cartão no formado DD/MM/AAAA
     * @param STRING $holderPhone = Telefone de contato junto com o DDD do Titular do Cartão (Sem pontuação)
     */
    public function createMemberShip($plan, $ref, $name, $email, $document, $phone, $street, $number, $complement, $district, $city, $state, $country, $postalCode, $token, $holderName, $holderBirth, $holderPhone)
    {
        $this->sAction = '/pre-approvals';
        $this->voParams = [
            'plan' => $plan,
            'reference' => $ref,
            'sender' => [
                'name' => $name,
                'email' => $email,
                'ip' => '1.1.1.1',
                'phone' => [
                    'areaCode' => substr($phone, 0, 2),
                    'number' => substr($phone, 2),
                ],
                'address' => [
                    'street' => $street,
                    'number' => $number,
                    'complement' => $complement,
                    'district' => $district,
                    'city' => $city,
                    'state' => $state,
                    'country' => $country,
                    'postalCode' => $postalCode,
                ],
                'documents' => [
                    ['type' => 'CPF', 'value' => $document],
                ],
            ],
            'paymentMethod' => [
                'type' => 'CREDITCARD',
                'creditCard' => [
                    'token' => $token,
                    'holder' => [
                        'name' => $holderName,
                        'birthDate' => $holderBirth,
                        'documents' => [
                            ['type' => 'CPF', 'value' => '23363265824'],
                        ],
                        'phone' => [
                            'areaCode' => substr($holderPhone, 0, 2),
                            'number' => substr($holderPhone, 2),
                        ],
                    ],
                ],
            ],
        ];
        $this->post();
    }

    /**
     * <b>getSessionId</b>: Método utilizado para resgatar o ID da sessão para que possa consumir as informações
     * do javascript nos métodos que manipulam o cartão de crédito.
     */
    public function getSessionId()
    {

        $this->sAction = 'sessions';
        $this->voParams = [
            'email' => $this->sEmail,
            'token' => $this->sToken,
        ];

        $ch = curl_init($this->sService.$this->sAction);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->voParams));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = simplexml_load_string(curl_exec($ch));
        curl_close($ch);
        $this->sSessionId = $result->id;
        echo json_encode($result);
    }

    /**
     * <b>getCallback</b>: Método responsável por retornar o objeto da comunicação REST
     */
    public function getCallback()
    {
        return $this->getCallback();
    }

    /**
     * <b>getMemberShip</b>: Método responsável por retornar o objeto de assinatura do PagSeguro
     * @param $sCode
     */
    public function getMemberShip($sCode)
    {
        $this->sAction = "/pre-approvals/notifications/{$sCode}";
        $this->get();
    }

    /**
     * <b>getTransaction</b>: Método responsável por retornar o objeto de transação do PagSeguro
     * @param $sCode
     */
    public function getTransaction($sCode)
    {
        $this->sAction = "transactions/{$sCode}";
        $this->get();
        return $this->sCallback;
    }

    /**
     * <b>get</b>: Método responsável por resgatar informações da comunicação REST
     */
    private function get()
    {
        $url = $this->sService.$this->sAction."?email={$this->sEmail}&token={$this->sToken}";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json', 'Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $this->sCallback = json_decode(curl_exec($ch));
        curl_close($ch);
        if (empty($this->callback)) {
            $url = $this->sService.$this->sAction."?email={$this->sEmail}&token={$this->sToken}";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json', 'Accept: application/xml;charset=ISO-8859-1'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $this->sCallback = simplexml_load_string(curl_exec($ch));
            curl_close($ch);
        }
    }

    /**
     * <b>post</b>: Método responsável por inputar informações na comunicação REST
     */
    private function post()
    {

        $sUrl = $this->sService.$this->sAction."?email={$this->sEmail}&token={$this->sToken}";

        $buildQuery = http_build_query($this->voParams);

        $curl = curl_init($sUrl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: application/x-www-form-urlencoded; charset=UTF-8"));
        curl_setopt($curl, CURLOPT_POST, true);
//        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $buildQuery);
        $retorno = curl_exec($curl);
//        $this->sCallback = simplexml_load_string(curl_exec($retorno));
        curl_close($curl);
        $xml = simplexml_load_string($retorno);

        if (!$xml->error){
            $sql = new Sql();

            $sql->select("CALL sp_TransacaoSave(:nPgoValor, :nPgoCod, :nEviId, :nPgsId, :nFpgId)", array(
                ":nPgoValor"=>$this->getItemAmount(),
                ":nPgoCod"=>$xml->code,
                ":nEviId"=>$_POST['EviId'],
                ":nPgsId"=>$xml->status,
                ":nFpgId"=>$xml->paymentMethod->type
            ));

            $sSql = "CALL sp_TransacaoSave({$this->getItemAmount()}, {$xml->code}, {$this->getEviId()}, {$xml->status}, {$xml->paymentMethod->type}";

            $sClass = "success";

            switch ($xml->status){
                case 2:
                    $sMsg = "Pagamento realizado com sucesso!";
                    break;
                case 3:
                    $sMsg = "Pagamento recusado pela operadora do cartão!";
                    $sClass = "danger";
                    break;
                default:
                    $sMsg = "Seu pagamento está sendo processado pela operadora do cartão de crédito, em breve você receberá um email com mais informações.";
                    $sClass = "warning";
            }

            echo json_encode(["sMsg"=>$sMsg,"sClass"=>$sClass,"sSql"=>$sSql]);
        } else{
            echo json_encode($xml);
        }


    }

    public function updateTransaction($nPgsId,$sPgoCod){

        $sql = new Sql();

        $sql->query("Update ev_inscricao_pagamento Set pgs_id = :nPgsId Where pgo_cod = :sPgoCod", [
            ":nPgsId"=>$nPgsId,
            ":sPgoCod"=>$sPgoCod
        ]);

    }

    public function listAll(){

        $sql = new Sql();

        return $sql->select("Select * From ev_inscricao_pagamento");

    }

}