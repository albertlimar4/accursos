<?php

namespace Hcode\Model;

use Exception;
use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getArtigoId()
 * @method getInscricaoId()
 * @method getEventoId()
 * @method getEventoNome()
 */
class Artigo extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM ev_lista_inscricoes WHERE ArtigoID IS NOT NULL GROUP BY Cpf");

    }

    public static function artigoPorInscricao($nPessoaFisicaId, $nEventoId){

        $sql = new Sql();

        $oArtigo = $sql->select("SELECT * FROM ev_artigo ea INNER JOIN ev_evento_inscricao eei on ea.ev_inscricao_id = eei.id WHERE eei.pessoa_fisica_id = :nPessoaFisicaId AND ev_evento_id = :nEventoId", [
            ":nPessoaFisicaId"=>$nPessoaFisicaId,
            ":nEventoId"=>$nEventoId
        ]);

        if ($oArtigo)
            return $oArtigo[0];
        else
            return false;

    }

    /**
     * @throws Exception
     */
    public function saveArtigo() {

        $nInscricaoId = $this->getInscricaoId();

        $sExtensao = pathinfo($_FILES['arquivo']['name'], PATHINFO_EXTENSION);

        $sNomeAluno = ucfirst(removeAcentos(mask_nome($_SESSION[User::SESSION]['PessoaNome'])));
        $sNomeAluno .= ucfirst(removeAcentos(mask_sobrenome($_SESSION[User::SESSION]['PessoaNome'])));

        $sEventoId = $this->getEventoId();

        $sEventoNome = str_replace(" ", "", ucwords(removeAcentos($this->getEventoNome())));

        $sNomeArquivo = $nInscricaoId.".".$sNomeAluno."-".$sEventoNome.".".$sExtensao;

        try {
            $sUrl = $this->moveFile($_FILES['arquivo'], $sEventoId, $sNomeArquivo);
        } catch (Exception $e) {
            throw new Exception("Erro ao salvar aquivo!");
        }

        $sql = new Sql();

        $sql->query("INSERT INTO ev_artigo (titulo, url, ev_inscricao_id) VALUES (:sTitulo, :sUrl, :nInscricaoId)", array(
            ":sTitulo"=>$sNomeArquivo,
            ":sUrl"=>$sUrl,
            ":nInscricaoId"=>$nInscricaoId
        ));

    }

    /**
     * @param $fArquivo
     * @param $nEventoId
     * @param $sNomeArquivo
     * @return string
     * @throws Exception
     */
    public function moveFile($fArquivo, $nEventoId, $sNomeArquivo){

        if ($fArquivo['error']){
            throw new Exception("Error: ".$fArquivo['error']);
        }

        $sDirUploads = "uploads".DIRECTORY_SEPARATOR."evento".DIRECTORY_SEPARATOR.$nEventoId;

        if (!is_dir($sDirUploads)){
            if(!mkdir($sDirUploads, 0755, true)) {
                throw new Exception("Não foi possível criar o diretório.");
            }
        }

        move_uploaded_file($fArquivo['tmp_name'], $sDirUploads.DIRECTORY_SEPARATOR.$sNomeArquivo);

        $sUrl = $sDirUploads.DIRECTORY_SEPARATOR.$sNomeArquivo;

        return $sUrl;

    }


}