<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Auxiliar extends Model{

    public static function listFormaPagamento(){

        $sql = new Sql();

        return $sql->select("Select * From aux_forma_pagamento Where fpg_ativo = 1");

    }

    public static function nivelEnsino(){

        $sql = new Sql();

        return $sql->select("Select * From aux_nivel_ensino Where nen_ativo = 1");

    }

    public static function rgEmissor() {

        $sql = new Sql();

        return $sql->select("Select * From aux_rg_emissor");

    }

    public static function listEscolaridade(){

        $sql = new Sql();

        return $sql->select("Select * From aux_escolaridade Order By esc_id");

    }

    public static function listEstadoCivil(){

        $sql = new Sql();

        return $sql->select("Select * From aux_estado_civil Order By eci_id");

    }

    public static function listPais(){

        $sql = new Sql();

        return $sql->select("Select * From aux_pais");

    }

    public static function listEstado(){

        $sql = new Sql();

        return $sql->select("Select * From aux_estado");

    }

    public static function listEstadoPorPais($sNomePais){

        $sql = new Sql();

        if ($sNomePais != "Brasil"){
            $sQuery = "Select * From aux_estado Where ISNULL(pai_id)";
        } else {
            $sQuery = "Select * From aux_estado est INNER JOIN aux_pais pai ON pai.pai_id = est.pai_id Where pai_nome = :sNomePais";
        }

        return $sql->select($sQuery, [
            "sNomePais"=>$sNomePais
        ]);

    }

    public static function listCidade(){

        $sql = new Sql();

        return $sql->select("Select * From aux_cidade Order By cid_nome");

    }

    public static function listCidadePorEstado($sEstSigla){

        $sql = new Sql();

        $sQuery = "Select * From v_cidades Where EstSigla = :sEstSigla Order By CidNome";

        return $sql->select($sQuery, [
            "sEstSigla"=>$sEstSigla
        ]);

    }

    public static function listProfissao(){

        $sql = new Sql();

        return $sql->select("Select * From aux_profissao Order By prs_descricao");

    }

    public static function listTitulo(){

        $sql = new Sql();

        return $sql->select("Select * From aux_titulo Order By tit_id");

    }

    public static function listInscricaoSituacao(){

        $sql = new Sql();

        return $sql->select("Select * From ev_inscricao_situacao");

    }

}