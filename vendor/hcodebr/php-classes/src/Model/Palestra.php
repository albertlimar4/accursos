<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Palestra extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM ev_lista_subeventos");

    }

    public static function listaPorEvento($idevento){

        $sql = new Sql();

        return $sql->select("SELECT * FROM ev_lista_subeventos WHERE ev_evento_id = :idevento", array(
            ":idevento"=>$idevento
        ));

    }

    public function save(){

        $sql = new Sql();

        $dInicio = maskData($this->getDtInicio());
        $dFim = maskData($this->getDtFim());
        if ($this->getVagas() == '')
            $nVagas = 0;
        else
            $nVagas = $this->getVagas();
        if ($this->getCh() == '')
            $nCh = 0;
        else
            $nCh = $this->getCh();

        $results = $sql->select("CALL sp_palestra_save(:tema, :descricao, :dinicio, :hinicio, :dfim, :hfim, :vagas, :ch, :evento, :categoria)", array(
            ":tema"=>$this->getTema(),
            ":descricao"=>$this->getDescricao(),
            ":dinicio"=>$dInicio,
            ":hinicio"=>$this->getHrInicio(),
            ":dfim"=>$dFim,
            ":hfim"=>$this->getHrFim(),
            ":vagas"=>$nVagas,
            ":ch"=>$nCh,
            ":evento"=>$this->getEvento(),
            ":categoria"=>$this->getCategoria()
        ));

        $this->setData($results[0]);

    }

    public function get($idcurso){

        $sql = new Sql();

        $results = $sql->select("SELECT id, tema, descricao, vagas, ch, ativo, imagem, ev_evento_id, ev_categoria_id, DATE_FORMAT(dt_inicio, '%d/%m/%Y') as dt_inicio, DATE_FORMAT(dt_fim, '%d/%m/%Y') as dt_fim, DATE_FORMAT(hr_inicio, '%H:%i') as hr_inicio, DATE_FORMAT(hr_fim, '%H:%i') as hr_fim FROM ev_palestra WHERE id = :id", array(
            ":id"=>$idcurso
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        $dInicio = maskData($this->getDtInicio());
        $dFim = maskData($this->getDtFim());

        if ($this->getVagas() == '')
            $nVagas = 0;
        else
            $nVagas = $this->getVagas();
        if ($this->getCh() == '')
            $nCh = 0;
        else
            $nCh = $this->getCh();
        
        $results = $sql->select("CALL sp_palestra_update(:id, :tema, :descricao, :dinicio, :hinicio, :dfim, :hfim, :vagas, :ch, :evento, :categoria)", array(
            ":id"=>$this->getId(),
            ":tema"=>$this->getTema(),
            ":descricao"=>$this->getDescricao(),
            ":dinicio"=>$dInicio,
            ":hinicio"=>$this->getHrInicio(),
            ":dfim"=>$dFim,
            ":hfim"=>$this->getHrFim(),
            ":vagas"=>$nVagas,
            ":ch"=>$nCh,
            ":evento"=>$this->getEvento(),
            ":categoria"=>$this->getCategoria()
        ));

        $this->setData($results[0]);

    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM ev_palestra WHERE id = :id", array(
            ":id"=>$this->getId()
        ));

    }

}