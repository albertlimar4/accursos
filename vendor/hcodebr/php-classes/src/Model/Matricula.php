<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;
use Hcode\Mailer;

class Matricula extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM ac_matriculas ORDER BY nome");

    }

    public static function listIncricoes(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM ev_lista_inscricoes WHERE UserId = :user ORDER BY EventoId ASC", array(
            ":user"=>$_SESSION[User::SESSION]["Id"]
        ));


    }

    public static function listAllCurso($idcurso){

        $sql = new Sql();

        return $sql->select("SELECT a.id as matriculaid, a.valor as matriculavalor, a.status as matriculastatus, a.cadastro as matriculacadastro, a.matricula_tipo as matriculatipo, a.ac_turmas_cursos_id as turma, b.id as alunoid, d.nome as alunonome, e.email as alunoemail FROM ac_matriculas a INNER JOIN ead_aluno b ON b.id = a.ac_alunos_id INNER JOIN ac_turmas_cursos c ON c.id = a.ac_turmas_cursos_id INNER JOIN pessoa_fisica d ON d.id = b.pessoa_fisica_id INNER JOIN pessoa e ON e.id = d.pessoa_id WHERE c.ac_cursos_id = :idcurso ORDER BY d.nome", array(
            ":idcurso"=>$idcurso
        ));

    }

    public function get($idmatricula){

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM ac_matriculas WHERE id = :id", array(
            ":id"=>$idmatricula
        ));

        $this->setData($results[0]);

    }

    public function verificaMatriculaExiste(){

        $verificaMatricula = new Sql();

        $result = $verificaMatricula->select("SELECT * FROM ac_matriculas WHERE ac_alunos_id = :aluno AND ac_turmas_cursos_id = :turma", array(
            ":aluno"=>$this->getaluno(),
            ":turma"=>$this->getturma()
        ));

        if ($result){
            return "O aluno já está matriculado nesse curso.";
        } else {
            return true;
        }

    }

    public function matricularAdm(){

        $this->verificaMatriculaExiste();

        if ($this->getturma() == ''){
            throw new \Exception("Selecione a turma.");
        }
        if ($this->getaluno() == ''){
            throw new \Exception("Aluno não carregado.");
        }

        $sql = new Sql();

        $matricula = date('Y').$this->getturma().($this->countNumMatriculas()+1);

        $valor = ($this->getvalor()) ? $this->getvalor() : 0;

        $results = $sql->select("CALL sp_alunos_matricular(:aluno, :turma, :valor, :tipo, :matricula)", array(
            ":aluno"=>$this->getaluno(),
            ":turma"=>$this->getturma(),
            ":valor"=>number_format($valor,2,'.',''),
            ":tipo"=>1,
            ":matricula"=>$matricula
        ));

        $this->setData($results[0]);

    }

    public function matricularGratis($idaluno, $idturma){

        $sql = new Sql();

        $matricula = date('Y').$idturma.($this->countNumMatriculas()+1);

        $results = $sql->select("CALL sp_alunos_matricular(:aluno, :turma, :valor, :tipo, :matricula)", array(
            ":aluno"=>$idaluno,
            ":turma"=>$idturma,
            ":valor"=>0,
            ":tipo"=>2,
            ":matricula"=>$matricula
        ));

        $this->setData($results[0]);

        return "Matricula realizada";

    }

    public function update(){

        $sql = new Sql();

        $sql->query("UPDATE ac_matriculas SET valor = :valor, ac_turmas_cursos_id = :turma WHERE id = :idmatricula", array(
            ":idmatricula"=>$this->getidmatricula(),
            ":valor"=>$this->getvalor(),
            ":turma"=>$this->getturma(),
        ));


    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM ac_matriculas WHERE id = :id", array(
            ":id"=>$this->getid()
        ));

    }

    public static function listCursos(){

        $sql = new Sql();

        return $sql->select("SELECT a.id as matriculaid, b.id as cursoid, b.nome as cursonome FROM ac_turmas_cursos a INNER JOIN ac_cursos b ON a.ac_cursos_id = b.id INNER JOIN ac_matriculas c ON c.ac_turmas_cursos_id = a.id INNER JOIN ead_aluno d ON d.id = c.ac_alunos_id INNER JOIN pessoa_fisica e ON e.id = d.pessoa_fisica_id INNER JOIN pessoa f ON f.id = e.pessoa_id INNER JOIN `user` g ON f.id = g.pessoa_id WHERE g.id = :iduser ORDER BY b.nome", array(
            ":iduser"=>$_SESSION[User::SESSION]["Id"]
        ));

    }

    public function countNumMatriculas(){

        $sql = new Sql();

        $ano = date('Y');

        $results = $sql->select("SELECT * FROM ac_matriculas WHERE matricula LIKE :ano", array(
            ":ano"=>$ano.'%'
        ));

        $matriculas = count($results);

        return $matriculas;

    }

    public static function listCursosGratuitosParaMatricular(){

        $sql = new Sql();

        return $sql->select("SELECT f.id as cursoid, f.nome as cursonome, a.id as turmaid FROM ac_turmas_cursos a INNER JOIN ac_cursos f ON f.id = a.ac_cursos_id WHERE NOT EXISTS(SELECT 1 FROM ac_matriculas b INNER JOIN ead_aluno c ON b.ac_alunos_id = c.id INNER JOIN pessoa_fisica g ON c.pessoa_fisica_id = g.id INNER JOIN pessoa h ON g.pessoa_id = h.id INNER JOIN user d ON h.id = d.pessoa_id INNER JOIN ac_turmas_cursos e ON e.id = b.ac_turmas_cursos_id  WHERE a.id = b.ac_turmas_cursos_id AND d.id = :iduser) AND f.tipo_matricula = 1 AND f.exibir = 1", array(
            ":iduser"=>$_SESSION[User::SESSION]["id"]
        ));

    }



    public static function vinculosAlunosAtivos($idaluno){
        $sql = new Sql();

        return $sql->select("SELECT c.nome as turma, a.nome as curso FROM ac_cursos AS a INNER JOIN ac_turmas_cursos AS b ON b.ac_cursos_id = a.id INNER JOIN ac_turmas AS c ON b.ac_turmas_id = c.id INNER JOIN ac_matriculas AS d ON d.ac_turmas_cursos_id = b.id WHERE d.ac_alunos_id = :idaluno", array(
            ":idaluno"=>$idaluno
        ));
    }

}