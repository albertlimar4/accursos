<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

class Acesso extends Model {

    public static function menuListAll() {

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM menu WHERE ativo = 1");

        return $results;

    }

    public static function menuAcesso($role){

        $sql = new Sql();

        $results = $sql->select("SELECT a.nome,a.icone,a.link,b.menu_id,b.user_role_id FROM menu AS a INNER JOIN menu_role AS b ON b.menu_id = a.id WHERE b.user_role_id = :role", array(
            ":role"=>$role
        ));

        return $results;

    }

    public function menuSave(){

        $sql = new Sql();

        $results = $sql->select("CALL sp_menu_save(:nome, :link, :icone)", array(
            ":nome"=>$this->getNome(),
            ":link"=>$this->getLink(),
            ":icone"=>$this->getIcone()
        ));

        return $results[0];

    }

    public function get($idmenu){

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM menu WHERE id = :id", array(
            ":id"=>$idmenu
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        $sql->query("CALL sp_menu_update(:id, :nome, :link, :icone)", array(
            ":id"=>$this->getId(),
            ":nome"=>$this->getNome(),
            ":link"=>$this->getLink(),
            ":icone"=>$this->getIcone()
        ));
    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM menu WHERE id = :id", array(
            ":id"=>$this->getId()
        ));

    }

    public function menuUpdate(){



    }

    public function menuDelete(){



    }

    public function menuAcessoLista(){



    }

    public function menuAcessoSave(){



    }

    public function menuAcessoUpdate(){



    }

    public function menuAcessoDelete(){



    }


}