<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getEndId()
 * @method getEndRua()
 * @method getEndNumero()
 * @method getEndComplemento()
 * @method getEndBairro()
 * @method getEndCidade()
 * @method getEndEstado()
 * @method getEndCep()
 * @method getPessoaId()
 * @method getPesId()
 * @method setEndId($end_id)
 */
class Endereco extends Model {

    public function save(){

        $sql = new Sql();

        if ($this->getEndCep() ==! '' && $this->getEndRua() ==! '') {

            $oEndereco = $sql->select("CALL sp_EnderecoSave (:sRua, :nNumero, :sComplemento, :sBairro, :sCidade, :sEstado, :sPais, :sCep, :nPesId)", array(
                ":sRua"=>$this->getEndRua(),
                ":nNumero"=>$this->getEndNumero(),
                ":sComplemento"=>$this->getEndComplemento(),
                ":sBairro"=>$this->getEndBairro(),
                ":sCidade"=>$this->getEndCidade(),
                ":sEstado"=>$this->getEndEstado(),
                ":sPais"=>"Brasil",
                ":sCep"=>$this->getEndCep(),
                ":nPesId"=>$this->getPesId()
            ));

            $this->setData($oEndereco[0]);

        }

    }

    public function get($nEndId){

        $sql = new Sql();

        $results = $sql->select("SELECT * FROM endereco WHERE end_id = :nEndId", array(
            ":nEndId"=>$nEndId
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $oEndereco = self::enderecoPorPessoa($this->getPesId());

        if (!$this->getEndId() && (!$oEndereco))
            $this->save();
        else{
            $sql = new Sql();

            $this->setEndId($oEndereco[0]['end_id']);

            $oEndereco = $sql->select("CALL sp_EnderecoUpdate (:nEndId, :sEndRua, :nEndNumero, :sEndComplemento, :sEndBairro, :sEndCidade, :sEndEstado, :sEndPais, :sEndCep)", array(
                ":nEndId"=>$this->getEndId(),
                ":sEndRua"=>$this->getEndRua(),
                ":nEndNumero"=>$this->getEndNumero(),
                ":sEndComplemento"=>$this->getEndComplemento(),
                ":sEndBairro"=>$this->getEndBairro(),
                ":sEndCidade"=>$this->getEndCidade(),
                ":sEndEstado"=>$this->getEndEstado(),
                ":sEndPais"=>"Brasil",
                ":sEndCep" => $this->getEndCep()
            ));

            $this->setData($oEndereco[0]);
        }

    }

    public static function enderecoPorPessoa($nPesId){

        $sql = new Sql();

        $oEndereco = $sql->select("Select * From endereco Where pes_id = :nPesId",[
            ":nPesId"=>$nPesId
        ]);

        return ($oEndereco) ? $oEndereco : false;

    }
}