<?php

    namespace Hcode\Model;

    use Hcode\DB\Sql;
    use Hcode\Model;
    use Hcode\Mailer;

    class Aluno extends Model {

        public static function listAll(){

            $sql = new Sql();

            return $sql->select("SELECT a.id AS id,  b.nome, b.sexo, b.cpf, b.rg, b.rg_emissor, b.nascimento, b.naturalidade, b.uf_natural, b.profissao, c.email, a.cadastro, b.estado_civil AS civil, b.escolaridade AS escolaridade, d.id AS userid,  d.login, d.password, d.user_role_id AS role, d.ativo, d.firstaccess FROM ead_aluno a INNER JOIN pessoa_fisica b ON a.pessoa_fisica_id = b.id INNER JOIN pessoa c ON b.pessoa_id = c.id INNER JOIN user d ON d.pessoa_id = c.id WHERE d.ativo = 1 ORDER BY b.nome");

        }

        public static function listInativos(){

            $sql = new Sql();

            return $sql->select("SELECT a.id AS id,  b.nome, b.sexo, b.cpf, b.rg, b.rg_emissor, b.nascimento, b.naturalidade, b.uf_natural, b.profissao, c.email, a.cadastro, b.estado_civil AS civil, b.escolaridade AS escolaridade, d.id AS userid,  d.login, d.password, d.user_role_id AS role, d.ativo, d.firstaccess FROM ead_aluno a INNER JOIN pessoa_fisica b ON a.pessoa_fisica_id = b.id INNER JOIN pessoa c ON b.pessoa_id = c.id INNER JOIN user d ON d.pessoa_id = c.id WHERE d.user_role_id = 2 AND d.ativo = 0 ORDER BY b.nome");

        }

        public function geraSenha($nome){

            $pass = removeAcentos(mask_nome(strtolower($nome))) .".".date('Y');
            return $pass;

        }

        public function geraLogin($login){
            $login = removeAcentos(mask_inv_cpf(removeEspacos($login)));

            return $login;
        }

        public function save(){

            if (!isset($_SESSION))session_start();

            $nome = trim(mb_strtoupper($this->getNome()), 'utf-8');
            $cpf = removeAcentos(mask_inv_cpf(removeEspacos($this->getCpf())));
            $cidade = trim(ucwords($this->getEndCidade()));
            $email = trim(strtolower($this->getEmail()));

            $dados = array(
                'nome' => $nome,
                'sexo' => $this->getSexo(),
                'ddd' => $this->getDdd(),
                'telefone' => $this->getTelefone(),
                'email' => $email,
                'cep' => $this->getCep(),
                'end_rua' => $this->getEndRua(),
                'end_numero' => $this->getEndNumero(),
                'end_complemento' => $this->getEndComplemento(),
                'end_bairro' => $this->getEndBairro(),
                'end_cidade' => $cidade,
                'end_estado' => $this->getEndEstado(),
                'evento' => $this->getEvento(),
                'workshop' => $this->getWorkshop(),
                'categoria' => $this->getCategoria(),
                'area' => $this->getArea(),
                'cpf' => $cpf,
                'password' => $this->getPassword1()
            );

            $_SESSION['Inscricao'] = $dados;


            if ($nome == '' || $cpf == '' ||$email == '' ){
                throw new \Exception("Os campos nome, cpf e email não podem ficar em branco.");
            }

            if (strlen($cpf) != 11){
                throw new \Exception("CPF inválido.");
            }

            $sql = new Sql();

            $oPessoa = $sql->select("SELECT * FROM pessoa_fisica WHERE cpf = :cpf", array(
                ":cpf"=>$cpf
            ));

            $voSituacao = $sql->select("SELECT id FROM ev_inscricao_situacao WHERE situacao = :situacao", array(
                ":situacao"=>"Pendente"
            ));

            $situacao = $voSituacao[0]['id'];

            $voRole = $sql->select("SELECT id FROM user_role WHERE descricao = :descricao", array(
                ":descricao"=>"Participante"
            ));

            $role = $voRole[0]['id'];

            if (!$oPessoa){
                $voInscricao = $sql->select("SELECT * FROM ev_inscricao AS a INNER JOIN pessoa_fisica AS b ON a.pessoa_fisica_id = b.id INNER JOIN pessoa AS c ON b.pessoa_id = c.id WHERE (b.cpf = :cpf OR c.email = :email) AND a.ev_evento_id = :evento OR a.ev_evento_id = :workshop", array(
                    ':cpf'=>$cpf,
                    ':email'=>$email,
                    ':evento' => $this->getEvento(),
                    ':workshop' => $this->getWorkshop()
                ));

                if (count($voInscricao) > 0){
                    throw new \Exception("Inscrição já realizada anteriormente.");
                }

                $password = password_hash($this->geraSenha($this->getPassword1()), PASSWORD_DEFAULT, [
                    "cost"=>12
                ]);

                $results = $sql->select("CALL sp_inscricao_save(:nome, :sexo, :ddd, :telefone, :email, :cep, :rua, :numero, :complemento, :bairro, :cidade, :uf, :evento, :situacao, :categoria, :area, :cpf)", array(
                    ':nome' => $nome,
                    ':sexo' => $this->getSexo(),
                    ':ddd' => $this->getDdd(),
                    ':telefone' => $this->getTelefone(),
                    ':email' => $email,
                    ':cep' => $this->getCep(),
                    ':rua' => $this->getEndRua(),
                    ':numero' => $this->getEndNumero(),
                    ':complemento' => $this->getEndComplemento(),
                    ':bairro' => $this->getEndBairro(),
                    ':cidade' => $cidade,
                    ':uf' => $this->getEndEstado(),
                    ':evento' => $this->getEvento(),
                    ':situacao' => $situacao,
                    ':categoria' => $this->getCategoria(),
                    ':area' => $this->getArea(),
                    ':cpf' => $cpf
                ));

                if ($this->getWorkshop()){

                    $oWorkshop = $sql->select("SELECT * FROM ev_evento_inscricao WHERE pessoa_fisica_id = :pessoa AND ev_evento_id = :workshop",array(
                        ":pessoa"=>$results[0]['pessoaFisicaId'],
                        ":workshop"=>$this->getWorkshop()
                    ));

                    if (!$oWorkshop){

                        $sql->query("INSERT INTO ev_evento_inscricao (area, categoria, pessoa_fisica_id, ev_evento_id, ev_inscricao_situacao_id) VALUES (:area, :categoria, :pessoa, :workshop, :situacao)", array(
                            ':area' => $this->getArea(),
                            ':categoria' => $this->getCategoria(),
                            ':pessoa'=>$results[0]['pessoaFisicaId'],
                            ':workshop'=>$this->getWorkshop(),
                            ':situacao' => $situacao
                        ));

                    }

                }

                if (!$results){
                    throw new \Exception("Não foi possível realizar a inscrição. Tente novamente mais tarde ou entre em contato conosco pelo e-mail accursoscontato@hotmail.com");
                } else {

                    $voUsuario = $sql->select("SELECT * FROM user WHERE login = :login OR email = :email", array(
                        ":login" => $cpf,
                        ":email" => $email
                    ));


                    if (!$voUsuario) {
                        $user = $sql->query("INSERT INTO user (login, email, password, pessoa_id, user_role_id) VALUES (:login, :email, :password, :pessoa, :role)", array(
                            ":login" => $cpf,
                            ":email" => $email,
                            ":password" => $password,
                            ":pessoa" => $results[0]['pessoaid'],
                            ":role" => $role
                        ));
                    }

                    $this->setData($results[0]);

                    $data = $results[0];

                    $url = $_SERVER['HTTP_HOST'];

                    $link = "http://$url/admin/login";

                    $mailer = new Mailer($email, mask_nome($nome), "Confirmação de Cadastro", "confirm", array(
                        "name" => mask_nome($nome),
                        "link" => $link
                    ));
                    unset($_SESSION['Inscricao']);

                    $mailer->send();

                }

            } else {

                if ($this->getEvento()){

                    $oEvento = $sql->select("SELECT * FROM ev_evento_inscricao WHERE pessoa_fisica_id = :pessoa AND ev_evento_id = :evento",array(
                        ":pessoa"=>$oPessoa[0]['id'],
                        ":evento"=>$this->getEvento()
                    ));

                    if (!$oEvento){

                        $sql->query("INSERT INTO ev_evento_inscricao (area, categoria, pessoa_fisica_id, ev_evento_id, ev_inscricao_situacao_id) VALUES (:area, :categoria, :pessoa, :evento, :situacao)", array(
                            ':area' => $this->getArea(),
                            ':categoria' => $this->getCategoria(),
                            ':pessoa'=>$oPessoa[0]['id'],
                            ':evento'=>$this->getEvento(),
                            ':situacao' => $situacao
                        ));
                    }
                }

                if ($this->getWorkshop()){

                    $oWorkshop = $sql->select("SELECT * FROM ev_evento_inscricao WHERE pessoa_fisica_id = :pessoa AND ev_evento_id = :workshop",array(
                        ":pessoa"=>$oPessoa[0]['id'],
                        ":workshop"=>$this->getWorkshop()
                    ));

                    if (!$oWorkshop){

                        $sql->query("INSERT INTO ev_evento_inscricao (area, categoria, pessoa_fisica_id, ev_evento_id, ev_inscricao_situacao_id) VALUES (:area, :categoria, :pessoa, :workshop, :situacao)", array(
                            ':area' => $this->getArea(),
                            ':categoria' => $this->getCategoria(),
                            ':pessoa'=>$oPessoa[0]['id'],
                            ':workshop'=>$this->getWorkshop(),
                            ':situacao' => $situacao
                        ));

                    }

                }

            }

        }

        public function get($idaluno){

            $sql = new Sql();

            $results = $sql->select("SELECT * FROM ead_aluno WHERE id = :id", array(
                ":id"=>$idaluno
            ));

            $this->setData($results[0]);

        }

        public function update(){

            $nome = trim(mb_strtoupper($this->getnome()), 'utf-8');
            $nascimento = maskData($this->getnascimento());
            $cpf = removeAcentos(mask_inv_cpf(removeEspacos($this->getcpf())));
            $orgao = str_replace("-", "/", trim(strtoupper($this->getorgao())));
            $cidade = trim(ucwords($this->getcidade()));
            $telefone = trim($this->gettelefone());
            $celular = trim($this->getcelular());
            $email = trim(strtolower($this->getemail()));

            if ($nascimento == "0000-00-00")$nascimento = null;

            if ($nome == '' || $nascimento == '' || $cpf == '' ||$email == '' ){
                throw new \Exception("Os campos em destaque não podem ficar em branco.");
            }

            if (strlen($cpf) != 11){
                throw new \Exception("CPF inválido.");
            }

            $sql = new Sql();


            $results = $sql->select("CALL sp_alunosupdate_save(:id, :nome, :sexo, :cpf, :rg, :orgao, :nascimento, :cidade, :uf, :civil, :profissao, :escolaridade, :telefone, :celular, :email, :login)", array(
                ":id"=>$this->getid(),
                ":nome"=>$nome,
                ":sexo"=>$this->getsexo(),
                ":cpf"=>$cpf,
                ":rg"=>$this->getrg(),
                ":orgao"=>$orgao,
                ":nascimento"=>$nascimento,
                ":cidade"=>$cidade,
                ":uf"=>$this->getuf(),
                ":civil"=>$this->getcivil(),
                ":profissao"=>$this->getprofissao(),
                ":escolaridade"=>$this->getescolaridade(),
                ":telefone"=>$telefone,
                ":celular"=>$celular,
                ":email"=>$email,
                ":login"=>$cpf
            ));

            if (!$results){
                throw new \Exception("Não foi possível atualizar.");
            } else {
                $this->setData($results[0]);
            }

        }

        public function updatePerfil($idaluno){

            $sql = new Sql();

            $results = $sql->select("CALL sp_perfilupdate_save(:id, :sexo, :rg, :orgao, :nascimento, :cidade, :uf, :civil, :profissao, :escolaridade, :telefone, :celular)", array(
                ":id"=>$idaluno,
                ":sexo"=>$this->getsexo(),
                ":rg"=>$this->getrg(),
                ":orgao"=>$this->getorgao(),
                ":nascimento"=>maskData($this->getnascimento()),
                ":cidade"=>$this->getcidade(),
                ":uf"=>$this->getuf(),
                ":civil"=>$this->getcivil(),
                ":profissao"=>$this->getprofissao(),
                ":escolaridade"=>$this->getescolaridade(),
                ":telefone"=>$this->gettelefone(),
                ":celular"=>$this->getcelular()
            ));

            if (!$results){
                throw new \Exception("Não foi possível atualizar seu perfil.");
            }

            $this->setData($results[0]);

        }

        public function updatePerfilFoto($file, $idaluno){

            if ($file['error']){
                throw new \Exception("Error: ".$file['error']);
            }

            $dirUploads = "res".DIRECTORY_SEPARATOR."admin".DIRECTORY_SEPARATOR."dist".DIRECTORY_SEPARATOR."img".DIRECTORY_SEPARATOR."users";

            if (!is_dir($dirUploads)){
                mkdir($dirUploads);
            }

            if (move_uploaded_file($file['tmp_name'], $dirUploads.DIRECTORY_SEPARATOR.$idaluno.".png")){
                $url = DIRECTORY_SEPARATOR.$dirUploads.DIRECTORY_SEPARATOR.$idaluno.".png";
            }

            $sql = new Sql();

            return $sql->select("UPDATE ac_alunos SET foto = :url WHERE id = :idaluno", array(
                ":url"=>$url,
                ":idaluno"=>$idaluno
            ));

        }

        public function delete(){

            $sql = new Sql();

            $sql->query("CALL sp_alunos_delete(:id)", array(
                ":id"=>$this->getid()
            ));

        }

        public function lock(){

            $sql = new Sql();

            $sql->query("CALL sp_alunos_lock_unlock(:id, :status)", array(
                ":id"=>$this->getid(),
                ":status"=>0
            ));

        }

        public function unlock(){

            $sql = new Sql();

            $sql->query("CALL sp_alunos_lock_unlock(:id, :status)", array(
                ":id"=>$this->getid(),
                ":status"=>1
            ));

        }

        public static function alunoCursos(){

            $sql = new Sql();

            $sql->select("SELECT * FROM ac_matriculas WHERE ac_alunos");

        }

        public static function getAlunoDocumento($aluno){

            $sql = new Sql();

            return $sql->select("SELECT id, documento, descricao, url, size, ac_alunos_id as alunoid, DATE_FORMAT(cadastro, '%d/%m/%Y %H:%i:%s') as cadastro FROM ac_aluno_documento WHERE ac_alunos_id = :aluno", array(
                ":aluno"=>$aluno
            ));

        }

        public static function getListAlunoDocumento(){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_aluno_documento ORDER BY ac_alunos_id, documento, cadastro");

        }

        public static function getCurso($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_cursos WHERE id = :id", array(
                ":id"=>$idcurso
            ));


        }

        public static function getModulos($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_modulos WHERE ac_cursos_id = :idcurso ORDER BY ordem, nome", array(
                ":idcurso"=>$idcurso
            ));
        }

        public static function getAulas($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT a.id as moduloid, a.nome as modulonome, a.ordem as moduloordem, b.id as aulaid, b.nome as aulanome, b.ordem as aulaordem, b.servidor as aulaservidor, b.url as aulaurl, b.video_id as video, c.id as cursoid FROM ac_modulos a INNER JOIN ac_aulas b ON b.ac_modulos_id = a.id INNER JOIN ac_cursos c ON a.ac_cursos_id = c.id WHERE c.id = :idcurso ORDER BY b.ordem, b.nome", array(
                ":idcurso"=>$idcurso
            ));

        }

        public static function getArquivos($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT a.id as moduloid, a.nome as modulonome, a.ordem as moduloordem, b.id as fileid, b.nome as filenome, b.descricao as filedescricao, b.url as fileurl, c.id as cursoid FROM ac_modulos a INNER JOIN ac_modulos_arquivos b ON b.ac_modulos_id = a.id INNER JOIN ac_cursos c ON a.ac_cursos_id = c.id WHERE c.id = :idcurso ORDER BY b.nome", array(
                ":idcurso"=>$idcurso
            ));

        }

        public static function getFile($idfile){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_modulos_arquivos WHERE id = :id", array(
                ":id"=>$idfile
            ));


        }

        public static function getAula($idaula){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_aulas WHERE id = :id", array(
                ":id"=>$idaula
            ));


        }

        public static function getAvaliacao($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT * FROM ac_avaliacoes WHERE ac_cursos_id = :idcurso", array(
                ":idcurso"=>$idcurso
            ));


        }

        public static function getAlunoLogado(){

            $sql = new Sql();

            return $sql->select("SELECT c.id as pessoaid, b.id as alunoid, a.nome as alunonome, a.sexo as alunosexo, a.cpf as alunocpf, a.rg as alunorg, a.rg_emissor as alunoorgao, DATE_FORMAT(a.nascimento, '%d/%m/%Y') as alunonascimento, a.naturalidade as alunocidade, a.uf_natural as alunouf, a.profissao as alunoprofissao, c.email as alunoemail, a.cadastro as alunocadastro, a.estado_civil as alunocivil, a.escolaridade as alunoescola FROM ead_aluno b INNER JOIN pessoa_fisica a ON b.pessoa_fisica_id = a.id INNER JOIN pessoa c ON a.pessoa_id = c.id INNER JOIN user d ON d.pessoa_id = c.id WHERE d.id = :user", array(
                ":user"=>$_SESSION[User::SESSION]["id"]
            ));

        }

        public static function getStatusAula($idaula){

            $sql = new Sql();

            return $sql->select("SELECT a.status as status, a.ac_aulas_id as aula FROM ac_matriculas_has_ac_aulas a INNER JOIN ac_matriculas b ON a.ac_matriculas_id = b.id INNER JOIN ead_aluno c ON c.id = b.ac_alunos_id INNER JOIN pessoa_fisica d ON d.id = c.pessoa_fisica_id INNER JOIN pessoa e ON e.id = d.pessoa_id INNER JOIN `user` f ON e.id = f.pessoa_id WHERE f.id = :iduser AND a.ac_aulas_id = :idaula", array(
                ":iduser"=>$_SESSION[User::SESSION]["id"],
                ":idaula"=>$idaula
            ));

        }

        public static function getStatusArquivo($idfile){

            $sql = new Sql();

            return $sql->select("SELECT a.status as status, a.ac_modulos_arquivos_id as file FROM ac_matriculas_has_ac_modulos_arquivos a INNER JOIN ac_matriculas b ON a.ac_matriculas_id = b.id INNER JOIN ead_aluno c ON c.id = b.ac_alunos_id INNER JOIN pessoa_fisica d ON d.id = c.pessoa_fisica_id INNER JOIN pessoa e ON e.id = d.pessoa_id INNER JOIN `user` f ON e.id = f.pessoa_id WHERE f.id = :iduser AND a.ac_modulos_arquivos_id = :idfile", array(
                ":iduser"=>$_SESSION[User::SESSION]["id"],
                ":idfile"=>$idfile
            ));

        }

        public static function getMatriculaLogado($idcurso){

            $sql = new Sql();

            return $sql->select("SELECT a.id as idmatricula, a.matricula as matricula FROM ac_matriculas a  INNER JOIN ead_aluno b ON b.id = a.ac_alunos_id INNER JOIN pessoa_fisica c ON b.pessoa_fisica_id = c.id INNER JOIN pessoa d ON c.pessoa_id = d.id INNER JOIN user e ON e.pessoa_id = d.id INNER JOIN ac_turmas_cursos f ON f.id = a.ac_turmas_cursos_id WHERE e.id = :iduser AND f.ac_cursos_id = :idcurso", array(
                ":iduser"=>$_SESSION[User::SESSION]["id"],
                ":idcurso"=>$idcurso
            ));

        }

        public static function setStatusAula($idaula, $idcurso){

            $matricula = self::getMatriculaLogado($idcurso);
            $matricula = $matricula[0]['idmatricula'];

            $sql = new Sql();

            $sql->select("INSERT INTO ac_matriculas_has_ac_aulas (ac_matriculas_id, ac_aulas_id, status) VALUES (:idmatricula, :idaula, :status)", array(
                ":idmatricula"=>$matricula,
                ":idaula"=>$idaula,
                ":status"=>1
            ));

        }

        public static function setStatusArquivo($idfile, $idcurso){

            $matricula = self::getMatriculaLogado($idcurso);
            $matricula = $matricula[0]['idmatricula'];

            $sql = new Sql();

            $sql->query("INSERT INTO ac_matriculas_has_ac_modulos_arquivos (ac_matriculas_id, ac_modulos_arquivos_id, status) VALUES (:idmatricula, :idfile, :status)", array(
                ":idmatricula"=>$matricula,
                ":idfile"=>$idfile,
                ":status"=>1
            ));

        }

        public static function setStatusAvaliacao($idav, $idcurso){

            $matricula = Aluno::getMatriculaLogado($idcurso);
            $matricula = $matricula[0]['idmatricula'];

            $sql = new Sql();

            return $sql->select("CALL sp_avaliacaonota_save (:idmatricula, :idavaliacao, :status)", array(
                ":idmatricula"=>$matricula,
                ":idavaliacao"=>$idav,
                ":status"=>0
            ));

        }

        public static function statusAulas($idcurso){

            $aulas = Aluno::getAulas($idcurso);

            $status = array();

            for ($i = 0; $i < count($aulas); $i++){
                $aulaid = $aulas[$i]['aulaid'];

                $statusAula = Aluno::getStatusAula($aulaid);

                if (count($statusAula) > 0){
                    $status[$aulaid] = $statusAula[0];
                } else {
                    $status[$aulaid]['status'] = 0;
                    $status[$aulaid]['aula'] = $aulaid;
                }

            }

            return $status;

        }

        public static function statusArquivos($idcurso){

            $files = Aluno::getArquivos($idcurso);

            $status = array();

            for ($i = 0; $i < count($files); $i++){
                $fileid = $files[$i]['fileid'];

                $statusArquivo = Aluno::getStatusArquivo($fileid);

                if (count($statusArquivo) > 0){
                    $status[$fileid] = $statusArquivo[0];
                } else {
                    $status[$fileid]['status'] = 0;
                    $status[$fileid]['file'] = $fileid;
                }

            }

            return $status;

        }

        public static function getProgressoCurso($idcurso){

            $matricula = Aluno::getMatriculaLogado($idcurso);
            $matricula = $matricula[0]['idmatricula'];

            $sql = new Sql();

            $recursos = $sql->select("SELECT (SELECT COUNT(*) FROM ac_aulas a INNER JOIN ac_modulos b ON a.ac_modulos_id = b.id WHERE b.ac_cursos_id = :curso) AS aulas, (SELECT COUNT(*) FROM ac_modulos_arquivos a INNER JOIN ac_modulos b ON a.ac_modulos_id = b.id WHERE b.ac_cursos_id = :curso) AS files, (SELECT COUNT(*) FROM ac_avaliacoes b WHERE ac_cursos_id = :curso) AS avaliacoes", array(
                ":curso"=>$idcurso
            ));

            $sql2 = new Sql();

            $concluido = $sql2->select("SELECT (SELECT COUNT(*) FROM ac_matriculas_has_ac_aulas a INNER JOIN ac_matriculas b ON a.ac_matriculas_id = b.id WHERE b.id = :matricula) AS aulas, (SELECT COUNT(*) FROM ac_matriculas_has_ac_modulos_arquivos a INNER JOIN ac_matriculas b ON a.ac_matriculas_id = b.id WHERE b.id = :matricula) AS files, (SELECT COUNT(*) FROM ac_matriculas_has_ac_avaliacoes WHERE ac_matriculas_id = :matricula AND `status` = 1 AND nota >= 7) AS avaliacoes", array(
                ":matricula"=>$matricula
            ));

            if ($concluido[0]['avaliacoes'] > 0){$av = 1;} else{$av = 0;}

            $sumRecursos = $recursos[0]['aulas'] + $recursos[0]['files'] + $recursos[0]['avaliacoes'];
            $sumConcluido = $concluido[0]['aulas'] + $concluido[0]['files'] + $av;

            $progresso = round($sumConcluido*100/$sumRecursos,0);

            return (int)$progresso;

        }

        public static function getProximaAula($idcurso){

            $user = $_SESSION[User::SESSION]["id"];

            $file = new Sql();

            $resultsFile = $file->select("SELECT a.id, a.nome FROM ac_modulos_arquivos a INNER JOIN ac_modulos h ON h.id = a.ac_modulos_id WHERE h.ac_cursos_id = :idcurso AND a.id NOT IN (SELECT b.id FROM ac_modulos_arquivos b INNER JOIN ac_modulos c ON c.id = b.ac_modulos_id INNER JOIN ac_matriculas_has_ac_modulos_arquivos d ON d.ac_modulos_arquivos_id = b.id INNER JOIN ac_matriculas e ON e.id = d.ac_matriculas_id INNER JOIN ead_aluno f ON f.id = e.ac_alunos_id INNER JOIN pessoa_fisica i ON i.id = f.pessoa_fisica_id INNER JOIN pessoa j ON j.id = i.pessoa_id INNER JOIN `user` g ON j.id = g.pessoa_id WHERE g.id = :iduser AND c.ac_cursos_id = :idcurso) ORDER BY a.nome LIMIT 1", array(
                ":idcurso"=>$idcurso,
                ":iduser"=>$user
            ));

            $resultsAula = $file->select("SELECT a.id, a.nome FROM ac_aulas a INNER JOIN ac_modulos h ON h.id = a.ac_modulos_id WHERE h.ac_cursos_id = :idcurso AND a.id NOT IN (SELECT b.id FROM ac_aulas b INNER JOIN ac_modulos c ON c.id = b.ac_modulos_id INNER JOIN ac_matriculas_has_ac_aulas d ON d.ac_aulas_id = b.id INNER JOIN ac_matriculas e ON e.id = d.ac_matriculas_id INNER JOIN ead_aluno f ON f.id = e.ac_alunos_id INNER JOIN pessoa_fisica i ON i.id = f.pessoa_fisica_id INNER JOIN pessoa j ON j.id = i.pessoa_id INNER JOIN `user` g ON j.id = g.pessoa_id WHERE g.id = :iduser AND c.ac_cursos_id = :idcurso) ORDER BY h.ordem, a.ordem LIMIT 1", array(
                ":idcurso"=>$idcurso,
                ":iduser"=>$user
            ));

            $resultsAv = $file->select("SELECT a.id, a.nome FROM ac_avaliacoes a WHERE a.ac_cursos_id = :idcurso AND a.id  NOT IN (SELECT b.id FROM ac_avaliacoes b INNER JOIN ac_matriculas_has_ac_avaliacoes d ON d.ac_avaliacoes_id = b.id INNER JOIN ac_matriculas e ON e.id = d.ac_matriculas_id INNER JOIN ead_aluno f ON f.id = e.ac_alunos_id INNER JOIN pessoa_fisica i ON i.id = f.pessoa_fisica_id INNER JOIN pessoa j ON j.id = i.pessoa_id INNER JOIN `user` g ON j.id = g.pessoa_id WHERE g.id = :iduser AND b.ac_cursos_id = :idcurso AND d.nota > 7) LIMIT 1", array(
                ":idcurso"=>$idcurso,
                ":iduser"=>$user
            ));

            if (isset($resultsFile[0]) && (count($resultsFile[0]) > 0)){
                $link = "/aluno/curso/file/".$idcurso."/".$resultsFile[0]['id'];
            } elseif (isset($resultsAula[0]) && (count($resultsAula[0]) > 0)){
                $link = "/aluno/curso/aula/".$idcurso."/".$resultsAula[0]['id'];
            } elseif (isset($resultsAv[0]) && (count($resultsAv[0]) > 0)){
                $link = "/aluno/curso/avaliacao/".$idcurso."/".$resultsAv[0]['id'];
            } else {
                $link = "/aluno/curso/".$idcurso;
            }

            return $link;

        }

        public function importarAlunos(){
            $arquivo = fopen('inscricoes2.csv','r');

            $soma = 1;

            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
                $dados = explode(';', $linha);

                if($dados[0] != 'id' && !empty($linha)){

                    $nome = mb_strtoupper($dados[1], 'utf8');
                    $sexo = $dados[2];
                    $cpf = mask_inv_cpf($dados[3]);
                    $rg = $dados[4];
                    $orgao = mb_strtoupper($dados[5], 'utf8');
                    $nascimento = $dados[6];
                    $nascimento = implode("-", array_reverse(explode("/", $nascimento)));
                    $cidade = ucfirst(strtolower($dados[7]));
                    $uf = strtoupper($dados[8]);
                    $civil = $dados[9];
                    $profissao = $dados[10];
                    $escolaridade = $dados[11];

                    $telefone = removeAcentos(removeEspacos($dados[12]));

                    $celular = removeAcentos(removeEspacos($dados[13]));

                    $email = strtolower($dados[14]);

                    $data = $dados[15]." ".$dados[16];

                    $pass = removeAcentos(mask_nome(strtolower($nome))) .".".date('Y');

                    $password = password_hash($pass, PASSWORD_DEFAULT, [
                        "cost"=>12
                    ]);

                    if ($cpf != ''){

                        $sql = new Sql();

                        $results = $sql->select("CALL sp_alunos_save(:nome, :sexo, :cpf, :rg, :orgao, :nascimento, :cidade, :uf, :civil, :profissao, :escolaridade, :telefone, :celular, :email, :data, :password, :login)", array(
                            ":nome"=>$nome,
                            ":sexo"=>$sexo,
                            ":cpf"=>$cpf,
                            ":rg"=>$rg,
                            ":orgao"=>$orgao,
                            ":nascimento"=>$nascimento,
                            ":cidade"=>$cidade,
                            ":uf"=>$uf,
                            ":civil"=>$civil,
                            ":profissao"=>$profissao,
                            ":escolaridade"=>$escolaridade,
                            ":telefone"=>$telefone,
                            ":celular"=>$celular,
                            ":email"=>$email,
                            ":data"=>$data,
                            ":password"=>$password,
                            ":login"=>$cpf
                        ));

                        $this->setData($results[0]);

                        $soma = $soma + 1;

                    }


                }
            }
        }

        public function resetPass($iduser, $password){

            $sql = new Sql();


            $sql->query("UPDATE `user` SET password = :password, firstaccess = NOW() WHERE id = :iduser", array(
                ":password"=>$password,
                ":iduser"=>$iduser
            ));

        }

        public function getAlunoPorUsuario($iduser){

            $sql = new Sql();

            $results = $sql->select("SELECT a.id as id, a.nome, b.id as userid FROM ac_alunos a INNER JOIN ac_users b ON a.id = b.ac_alunos_id WHERE b.id = :iduser LIMIT 1", array(
                ":iduser"=>$iduser
            ));

            $this->setData($results[0]);

        }

        public function getAlunosPagina($page = 1, $itemsPerPage = 25){

            $start = ($page - 1) * $itemsPerPage;

            $sql = new Sql();

            $results = $sql->select("SELECT SQL_CALC_FOUND_ROWS a.id as id, a.nome, a.sexo, a.cpf, a.rg, a.orgao, a.nascimento, a.cidade, a.uf, a.profissao, a.telefone, a.celular, a.email, a.cadastro, a.foto, a.estado_civil as civil, a.escolaridade as escolaridade, b.id as userid, b.login, b.password, b.ac_roles_id as role, b.status, b.firstaccess FROM ac_alunos a INNER JOIN ac_users b ON a.id = b.ac_alunos_id WHERE b.ac_roles_id = 2 ORDER BY nome LIMIT $start, $itemsPerPage");

            $total = $sql->select("SELECT FOUND_ROWS() AS total");

            return [
                "alunos"=>$results,
                "total"=>(int)$total[0]['total'],
                "pages"=>ceil($total[0]['total'] / $itemsPerPage)
            ];



        }

    }