<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getTelDdi()
 * @method getTelDdd()
 * @method getTelNumero()
 * @method getTtiId()
 * @method getPesId()
 * @method getTelId()
 */
class Telefone extends Model {

    public static function listTipo(){

        $sql = new Sql();

        return $sql->select("Select * From aux_telefone_tipo Order By tti_id");

    }

    public function save(){

        $sql = new Sql();

            if (!$this->getTelId() && !self::telefonePorPessoa($this->getPesId())){
                $sql->select("CALL sp_TelefoneSave (:nTelDdi, :nTelDdd, :nTelNumero, :nTtiId, :nPesId)", array(
                    ":nTelDdi" => $this->getTelDdi(),
                    ":nTelDdd" => $this->getTelDdd(),
                    ":nTelNumero" => $this->getTelNumero(),
                    ":nTtiId" => $this->getTtiId(),
                    ":nPesId" => $this->getPesId()
                ));
            }

    }

    public function get($nTelId){

        $sql = new Sql();

        $results = $sql->select("Select * From telefone Where tel_id = :nTelId", array(
            ":nTelId"=>$nTelId
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        if (!$this->getTelId() && $this->getTelNumero())
            $this->save();
        elseif($this->getTelId()) {
            $sql->select("CALL sp_TelefoneUpdate (:nTelId, :nTelDdi, :nTelDdd, :nTelNumero, :nTtiId)", array(
                ":nTelId"=>$this->getTelId(),
                ":nTelDdi"=>$this->getTelDdi(),
                ":nTelDdd"=>$this->getTelDdd(),
                ":nTelNumero"=>$this->getTelNumero(),
                ":nTtiId"=>$this->getTtiId()
            ));
        }
    }

    public function delete(){

        $sql = new Sql();

        $sql->select("Delete From telefone Where tel_id = :nTelId", array(
            ":nTelId"=>$this->getTelId()
        ));

    }

    public static function listTelefoneTipo(){

        $sql = new Sql();

        return $sql->select("Select * From aux_telefone_tipo Order By tti_descricao");

    }

    public static function telefonePorPessoa($nPesId){

        $sql = new Sql();

        return $sql->select("Select * From telefone Where pes_id = :nPesId Limit 1", array(
            ":nPesId"=>$nPesId
        ));

    }

}