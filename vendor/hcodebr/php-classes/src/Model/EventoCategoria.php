<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;

/**
 * @method getCatDescricao()
 * @method getCatId()
 */
class EventoCategoria extends Model {

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("Select * From ev_evento_categoria Order By cat_descricao");

    }

    public function save(){

        $sql = new Sql();

        $vResult = $sql->select("Insert Into ev_evento_categoria (cat_descricao) Values (:sCatDescricao)", array(
            ":sCatDescricao"=>$this->getCatDescricao()
        ));

        if ($vResult[0] == 0)
            return ["Categoria cadastrada com sucesso","success"];
        else
            return ["Erro ao cadastrar Categoria. Código: {$vResult[0]}","danger"];

    }

    public function get($nCatId){

        $sql = new Sql();

        $results = $sql->select("Select * From ev_evento_categoria Where cat_id = :nCatId", array(
            ":nCatId"=>$nCatId
        ));

        $this->setData($results[0]);

    }

    public function update(){

        $sql = new Sql();

        $vResult = $sql->query("Update ev_evento_categoria SET cat_descricao = :sCatDescricao Where cat_id = :nCatId", [
            ":nCatId"=>$this->getCatId(),
            ":sCatDescricao"=>$this->getCatDescricao()
        ]);

        if ($vResult[0] == 0)
            return ["Categoria alterada com sucesso","success"];
        else
            return ["Erro ao alterar Categoria. Código: {$vResult[0]}","danger"];
    }

    public function delete(){

        $sql = new Sql();

        $vResult = $sql->query("Delete From ev_evento_categoria Where cat_id = :nCatId", array(
            ":nCatId"=>$this->getCatId()
        ));

        if ($vResult[0] == 0)
            return ["Categoria excluída com sucesso","success"];
        else
            return ["Erro ao excluir Categoria. Código: {$vResult[0]}","danger"];

    }

}