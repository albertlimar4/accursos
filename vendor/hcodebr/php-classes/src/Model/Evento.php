<?php

namespace Hcode\Model;

use Hcode\DB\Sql;
use Hcode\Model;
use Exception;
use Hcode\Upload;

/**
 * @method getEveId()
 * @method getEveNome()
 * @method getEveDescricao()
 * @method getEveInicio()
 * @method getEveFim()
 * @method getEveVagas()
 * @method getCatId()
 * @method setEveInicioFormatado(string $maskData)
 * @method setEveFimFormatado(string $maskData)
 * @method getEveInicioFormatado()
 * @method getEveFimFormatado()
 * @method getEventoPai()
 * @method setEventoPai($param)
 * @method getValId()
 * @method getValDescricao()
 * @method getEveDestaque()
 * @method getEveValor()
 */

class Evento extends Model {

    public static function listEventosPrincipais(){

        $sql = new Sql();

        return $sql->select("Select * From v_eventos Where isnull(EventoPai) And EveAtivo = 1");

    }

    public static function listEventosDestaque(){

        $sql = new Sql();

        return $sql->select("Select * From v_eventos Where isnull(EventoPai) And EveAtivo = 1 And EveImagem Is Not Null And EveDestaque = 1 And EveFim > Now()");

    }

    public static function listEventosSite(){

        $sql = new Sql();

        return $sql->select("Select * From v_eventos Where isnull(EventoPai) And EveAtivo = 1 And EveImagem Is Not Null And EveFim > Now() Order By EveInicio");

    }

    public function listSubEventos($nEveId){

        $sql = new Sql();

        return $sql->select("Select * From v_eventos Where EventoPai = :nEveId", array(
            ":nEveId"=>$nEveId
        ));

    }

    public static function listAll(){

        $sql = new Sql();

        return $sql->select("Select * From v_eventos");

    }

    public function get($nEveId){

        $sql = new Sql();

        $oEvento = $sql->select("Select * From v_eventos Where EveId = :nEveId", array(
            ":nEveId"=>$nEveId
        ));

        if ($oEvento) {
            $this->setData($oEvento[0]);
            return true;
        }
        else
            return false;

    }

    public function save(){

        $sql = new Sql();

        $_SESSION['voCadastro'] = $this;

        $this->setEveInicioFormatado(maskData($this->getEveInicio(), true));
        $this->setEveFimFormatado(maskData($this->getEveFim(), true));

        $this->setEventoPai(($this->getEventoPai()) ? $this->getEventoPai() : null);

        $oEvento = $sql->select("CALL sp_EventoSave(:sEveNome, :sEveDescricao, :dEveInicio, :dEveFim, :nEveValor, :nEveDestaque, :nCatId, :nEventoPai)", array(
            ":sEveNome"=>$this->getEveNome(),
            ":sEveDescricao"=>$this->getEveDescricao(),
            ":dEveInicio"=>$this->getEveInicioFormatado(),
            ":dEveFim"=>$this->getEveFimFormatado(),
            ":nEveValor"=>formatDecimal($this->getEveValor()),
            ":nEveDestaque"=>$this->getEveDestaque(),
            ":nCatId"=>$this->getCatId(),
            ":nEventoPai"=>$this->getEventoPai()
        ));

        if ($_FILES['EveImagem']['name']) {
            $sUrl = $this->uploadImagem();

            if (!is_array($sUrl))
                $sql->query("Update ev_evento Set eve_imagem = :sEveImagem Where eve_id = :nEveId", [
                    ":nEveId" => $oEvento[0]['eve_id'],
                    ":sEveImagem" => $sUrl
                ]);
            else
                return $sUrl;
        }

        if ($oEvento)
            return ["Evento cadastrado com sucesso!","alert alert-success"];
        else
            return ["Erro ao cadastrar evento!","alert alert-danger"];
    }

    public function update(){

        $sql = new Sql();

        $this->setEveInicioFormatado(maskData($this->getEveInicio(), true));
        $this->setEveFimFormatado(maskData($this->getEveFim(), true));

        $this->setEventoPai(($this->getEventoPai()) ? $this->getEventoPai() : null);

        $oEvento = $sql->select("CALL sp_EventoUpdate(:nEveId, :sEveNome, :sEveDescricao, :dEveInicio, :dEveFim, :nEveValor, :nEveDestaque, :nCatId)", array(
            ":nEveId"=>$this->getEveId(),
            ":sEveNome"=>$this->getEveNome(),
            ":sEveDescricao"=>$this->getEveDescricao(),
            ":dEveInicio"=>$this->getEveInicioFormatado(),
            ":dEveFim"=>$this->getEveFimFormatado(),
            ":nEveValor"=>formatDecimal($this->getEveValor()),
            ":nEveDestaque"=>($this->getEveDestaque()) ? $this->getEveDestaque() : 0,
            ":nCatId"=>$this->getCatId()
        ));

        if ($_FILES['EveImagem']['name']) {
            $sUrl = $this->uploadImagem();

            if (!is_array($sUrl))
                $sql->query("Update ev_evento Set eve_imagem = :sEveImagem Where eve_id = :nEveId", [
                    ":nEveId" => $oEvento[0]['eve_id'],
                    ":sEveImagem" => $sUrl
                ]);
            else
                return $sUrl;
        }

        if ($oEvento)
            return [0=>"Evento alterado com sucesso!",1=>"alert alert-success"];
        else
            return [0=>"Erro ao alterar evento!",1=>"alert alert-danger"];

    }

    public function uploadImagem(){

        $sDirUploads = "uploads/img";

        $oUpload = new Upload(null, 3500000, $sDirUploads, $this->getEveId());

        return $oUpload->putFile($_FILES['EveImagem'],$this->getEveId());

    }

    public function delete(){

        $sql = new Sql();

        $sql->query("Delete From ev_evento Where eve_id = :nEveId", array(
            ":nEveId"=>$this->getEveId()
        ));

    }

    public static function listInscricoes($nEveId) {

        $sql = new Sql();

        return $sql->select("Select * From v_inscricoes Where EveId = :nEveId Order By PfiNome", array(
            ":nEveId"=>$nEveId
        ));

    }

    public function novaInscricao() {

        $sql = new Sql();

        $voSituacao = $sql->select("Select sit_id From ev_inscricao_situacao Where sit_descricao = :sSitDescricao", array(
            ":sSitDescricao"=>"Pendente"
        ));

        $situacao = $voSituacao[0]['id'];

        foreach ($this->getEvento() as $evento){

            $sql->query("INSERT INTO ev_evento_inscricao (area, categoria, pessoa_fisica_id, ev_evento_id, ev_inscricao_situacao_id) VALUES (:area, :categoria, :pessoa, :evento, :situacao)", array(
                ":area"=>$this->getArea(),
                ":categoria"=>$this->getCategoria(),
                ":pessoa"=>$this->getAluno(),
                ":evento"=>$evento,
                ":situacao"=>$situacao
            ));

        }
    }

}