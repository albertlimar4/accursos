<?php

use Hcode\Model\Auxiliar;
use \Hcode\Page;


$app->get('/estados/:sPaiNome', function($sPaiNome) {

    $voEstado = Auxiliar::listEstadoPorPais($sPaiNome);

    $page = new Page([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("estados", array(
        "voEstado"=>$voEstado
    ));

});

$app->get('/cidades/:sEstSigla', function($sEstSigla) {

    $voCidade = Auxiliar::listCidadePorEstado($sEstSigla);

    $page = new Page([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("cidades", array(
        "voCidade"=>$voCidade
    ));

});