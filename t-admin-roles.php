<?php

use Hcode\Model\Menu;
use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Role;

$app->get('/admin/roles', function() {

    User::verifyLogin();

    $voRoles = Role::listAll();

    $voMenu = Menu::menuListAll();

    $page = new PageAdmin();

    $page->setTpl("roles", array(
        "voRoles"=>$voRoles,
        "voMenu"=>$voMenu
    ));

});

$app->get('/admin/roles/create', function() {

    User::verifyLogin();

    $page = new PageAdmin();

    $page->setTpl("roles-create");

});

$app->get('/admin/roles/:idcategoria/delete', function($idrole) {

    User::verifyLogin();

    $role = new Role();

    $role->get((int)$idrole);

    $role->delete();

    header("Location: /admin/roles");
    exit();

});

$app->get('/admin/roles/:idcategoria', function($idrole) {

    User::verifyLogin();

    $role = new Role();

    $role->get((int)$idrole);

    $page = new PageAdmin();

    $page->setTpl("roles-update", array(
        "role"=>$role->getValues(),
    ));

});

$app->post('/admin/roles/create', function (){

    User::verifyLogin();

    $role = new Role();

    $role->setData($_POST);

    $role->save();

    header("Location: /admin/roles");
    exit();

});

$app->post('/admin/roles/:idcategoria', function($idrole) {

    User::verifyLogin();

    $role = new Role();

    $role->get((int)$idrole);

    $role->setData($_POST);

    $role->update();

    header("Location: /admin/roles");
    exit();

});



