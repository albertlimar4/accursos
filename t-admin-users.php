<?php

use Hcode\Model\Pessoa;
use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Role;

$app->get('/admin/users', function() {

    User::verifyLogin();

    $voUser = User::listAll();

    $page = new PageAdmin();

    $page->setTpl("users", array(
        "voUser"=>$voUser
    ));

});

$app->get('/admin/users/inativos', function() {

    User::verifyLogin();

    $voUser = User::listAll(0);

    $page = new PageAdmin();

    $page->setTpl("users", array(
        "voUser"=>$voUser
    ));

});

$app->get('/admin/users/create', function() {

    User::verifyLogin();

    $voRole = Role::listAll();

    $page = new PageAdmin();

    $page->setTpl("users-create",[
        "voRole"=>$voRole
    ]);

});

$app->post('/admin/users/create', function() {

    User::verifyLogin();

    $oPessoa = new Pessoa();
    $oPessoa->setData($_POST);
    $oPessoa->savePessoaFisica();

    $oUser = new User();
    $oUser->setData($oPessoa->getValues());
    $oUser->save();

    header("Location: /admin/users");
    exit();

});

$app->get('/admin/users/:nUserId', function($nUserId) {

    User::verifyLogin();

    $voRoles = Role::listAll();

    $oUser = new User();

    $oUser->get((int)$nUserId);

    $page = new PageAdmin();

    $page->setTpl("users-update", array(
        "oUser"=>$oUser->getValues(),
        "voRoles"=>$voRoles
    ));

});

$app->get('/admin/users/:nUserId/lock', function($nUserId) {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get((int)$nUserId);

    $oUser->lockUnlock(0);

    header("Location: /admin/users");
    exit();

});

$app->get('/admin/users/:nUserId/unlock', function($nUserId) {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get((int)$nUserId);

    $oUser->lockUnlock(1);

    header("Location: /admin/users");
    exit();

});

$app->post('/admin/users/:nUserId', function($nUserId) {

    User::verifyLogin();

    $oUser = new User();

    $oUser->get((int)$nUserId);

    $oUser->setData($_POST);

    $oUser->update();

    header("Location: /admin/users");
    exit();

});