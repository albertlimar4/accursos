<?php

use Hcode\Model;
use Hcode\Model\EventoCategoria;
use Hcode\Model\User;
use Hcode\PageAdmin;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/admin/categorias', function() {

    User::verifyLogin();

    $voCategoria = EventoCategoria::listAll();

    $page = new PageAdmin();

    $page->setTpl("categorias", array(
        "voCategoria"=>$voCategoria,
        "vError"=>Model::getError()
    ));

});

$app->post('/admin/categorias/create', function (){

    User::verifyLogin();

    $oCategoria = new EventoCategoria();

    $oCategoria->setData($_POST);

    try{
        Model::setError($oCategoria->save());
    } catch (Exception $e) {
        Model::setError($e->getMessage());
    }

    header("Location: /admin/categorias");
    exit();

});

$app->post('/admin/categoria/update', function() {

    User::verifyLogin();

    $oCategoria = new EventoCategoria();

    $oCategoria->setData($_POST);

    try{
        Model::setError($oCategoria->update());
    } catch (Exception $e) {
        Model::setError($e->getMessage());
    }

    header("Location: /admin/categorias");
    exit();

});

$app->get('/admin/categoria/:nCatId/delete', function($nCatId) {

    User::verifyLogin();

    $oCategoria = new EventoCategoria();

    $oCategoria->get((int)$nCatId);

    try{
        Model::setError($oCategoria->delete());
    } catch (Exception $e) {
        Model::setError($e->getMessage());
    }

    header("Location: /admin/categorias");
    exit();

});