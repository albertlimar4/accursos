<?php

use Hcode\Model\Auxiliar;
use Hcode\Model\Evento;
use Hcode\Model\Financeiro;
use Hcode\Model\Inscricao;
use Hcode\Model\Telefone;
use Hcode\Model\User;
use Hcode\PageAdmin;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/admin/evento/inscricoes/:nEveId', function($nEveId) {

    User::verifyLogin();

    $voInscricao = Evento::listInscricoes($nEveId);

    $oEvento = new Evento();
    $oEvento->get($nEveId);

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-inscricoes", array(
        "oEvento"=>$oEvento->getValues(),
        "voInscricao"=>$voInscricao
    ));

});

$app->get('/admin/evento/inscricao/:nEviId/delete', function($nEviId) {

    User::verifyLogin();

    $oInscricao = new Inscricao();

    $oInscricao->deleteInscricao($nEviId);

});

$app->get('/admin/evento/inscricao/update/form/:nEviId', function($nEviId) {

    User::verifyLogin();

    $oInscricao = new Inscricao();

    $oInscricao->get($nEviId);

    $voTelefone = Telefone::telefonePorPessoa($oInscricao->getPesId());

    $voTelefoneTipo = Telefone::listTelefoneTipo();

    $voEstadoCivil = Auxiliar::listEstadoCivil();

    $voProfissao = Auxiliar::listProfissao();

    $voEstado = Auxiliar::listEstado();

    $voCidade = Auxiliar::listCidadePorEstado($oInscricao->getPfiUfNatural());

    $voEscolaridade = Auxiliar::listEscolaridade();

    $voSituacao = Auxiliar::listInscricaoSituacao();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-inscricao-form", array(
        "oInscricao"=>$oInscricao->getValues(),
        "voTelefone"=>$voTelefone,
        "voTelefoneTipo"=>$voTelefoneTipo,
        "voEscolaridade"=>$voEscolaridade,
        "voEstadoCivil"=>$voEstadoCivil,
        "voCidade"=>$voCidade,
        "voEstado"=>$voEstado,
        "voProfissao"=>$voProfissao,
        "voSituacao"=>$voSituacao
    ));

});

$app->post('/admin/evento/inscricao/update', function() {

    User::verifyLogin();

    $oInscricao = new Inscricao();

    $oInscricao->setData($_POST);

    $oInscricao->update();

    echo json_encode(["msg"=>"Inscrição alterada com sucesso!", "class"=>"alert alert-success", "EveId"=>$oInscricao->getEveId()]);

});

$app->post('/admin/eventos/inscricao/pagamento', function() {

    User::verifyLogin();

    $evento = $_POST['evento'];

    $financeiro = new Financeiro();

    $financeiro->setData($_POST);

    $financeiro->savePgto();

    header("Location: /admin/eventos/inscricoes/$evento");
    exit();

});