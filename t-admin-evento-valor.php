<?php

/** @noinspection PhpUndefinedVariableInspection */

use Hcode\Model\EventoValor;
use Hcode\Model\User;
use Hcode\PageAdmin;

$app->get('/admin/evento/valor/:nEveId', function($nEveId) {

    User::verifyLogin();

    $voEventoValor = EventoValor::valoresPorEventoAtivo($nEveId);

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-valor", array(
        "voEventoValor"=>$voEventoValor
    ));

});

$app->post('/admin/evento/valor/create', function() {

    User::verifyLogin();

    $oEventoValor = new EventoValor();

    $oEventoValor->setData($_POST);

    echo json_encode($oEventoValor->save());

});

$app->post('/admin/evento/valor/update', function() {

    User::verifyLogin();

    $oEventoValor = new EventoValor();

    $oEventoValor->setData($_POST);

    echo json_encode($oEventoValor->update());

});

$app->get('/admin/eventos/lote/:idevento/:idlote/delete', function($nEventoId,$nEventoValorId) {

    User::verifyLogin();

    $lote = new EventoValor();

    $lote->deleteEventoValor($nEventoValorId);

    header("Location: /admin/eventos/$nEventoId");
    exit();

});