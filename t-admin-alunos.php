<?php

use Hcode\Model\Inscricao;
use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Evento;

$app->get('/admin/alunos', function (){

    User::verifyLogin();

    $voUsuario = User::listAll();

    $voEvento = Evento::listAll();

    $page = new PageAdmin();

    $page->setTpl("alunos", array(
        "voUsuario"=>$voUsuario,
        "voEvento"=>$voEvento,
        "errorRegister"=>User::getErrorRegister()
    ));

});

$app->post('/admin/alunos/inscricao', function (){

    User::verifyLogin();

    $oInscricao = new Inscricao();

    $oInscricao->setData($_POST);

    try{

        $oInscricao->saveInscricao();

    } catch (Exception $e) {

        User::setErrorRegister($e->getMessage());
    }

    header("Location: /admin/alunos");
    exit();

});