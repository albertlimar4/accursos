<?php
header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");

use Hcode\DB\Sql;

require_once("vendor/autoload.php");
include_once "functions.php";

$sql = new Sql();

$vResult = $sql->query("Insert Into pagseguro_notifica (pno_code, pno_type) Values (:sPnoCode, :sPnoType)", [
    ":sPnoCode"=>$_POST['notificationCode'],
    ":sPnoType"=>$_POST['notificationType']
]);

gravar("{$_POST['notificationCode']}\n");