<?php

use Hcode\Model;
use \Hcode\PageAdmin;
use \Hcode\Model\User;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/admin', function() {

    User::verifyLogin();

    $page = new PageAdmin();

    $page->setTpl("index");

});

$app->get('/admin/login', function() {

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("login", array(
        "vError"=>Model::getError()
    ));

});

$app->post('/admin/login', function (){

    try{

        User::login($_POST['UseLogin'], $_POST['UseSenha']);

    } catch (Exception $e) {

        Model::setError($e->getMessage());
    }

    header("Location: /admin/login");
    exit();

});

$app->get('/admin/logout', function() {

    User::logout();

    header("Location: /admin/login");
    exit();

});

$app->get('/admin/forgot', function() {

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("forgot", array(
        "vError"=>User::getError()
    ));

});

$app->post('/admin/forgot', function() {

    try{

        User::getForgot($_POST['UseEmail'], $_POST['PfiCpf']);

        header("Location: /admin/forgot/sent");
        exit();

    } catch (Exception $e) {

        Model::setError($e->getMessage());
        header("Location: /admin/forgot");
        exit();
    }

});

$app->get("/admin/forgot/sent", function (){

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("forgot-sent");

});

$app->get("/admin/forgot/reset", function (){

    $nUreId = str_replace(" ", "+", $_GET['sCode']);

    $oUser = User::validForgotDecrypt($nUreId);

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("forgot-reset", array(
        "sPfiNome"=>(isset($oUser)) ? $oUser['PfiNome'] : "",
        "nUreId"=>$nUreId,
        "vError"=>Model::getError()
    ));

});

$app->post("/admin/forgot/reset", function (){

    $oUserRecovery = User::validForgotDecrypt($_POST['UreId']);

    $oUser = new User();

    $oUser->get((int)$oUserRecovery['UseId']);

    $sUseSenha = password_hash($_POST['UseSenha'], PASSWORD_DEFAULT, [
        "cost"=>12
    ]);

    $oUser->updatePassword($sUseSenha, $oUserRecovery['UseId']);

    User::setForgotUsed($oUserRecovery["UreId"]);

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("forgot-reset-success", [
        "vError"=>Model::getError()
    ]);

});

$app->get('/admin/password', function() {

    session_start();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("password-change");

});

$app->post('/admin/password', function() {

    session_start();

    $sUseSenha = password_hash($_POST['UseSenha'], PASSWORD_DEFAULT, [
        "cost"=>12
    ]);

    $nUseId = $_SESSION[User::SESSION]['UseId'];

    $oUser = new User();

    $oUser->updatePassword($sUseSenha, $nUseId);

    header("Location: /admin/login");
    exit();

});