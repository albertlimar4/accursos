<?php

use \Hcode\PageAluno;
use \Hcode\Model\User;
use \Hcode\Model\Artigo;
use \Hcode\Model\Matricula;

$app->get('/aluno', function() {

    User::verifyLoginaluno();

    $voInscricoes = Matricula::listIncricoes();

    $page = new PageAluno();

    $page->setTpl("index", array(
        "voInscricoes"=>$voInscricoes
    ));

});

$app->post('/aluno/artigo', function() {

    User::verifyLoginaluno();

    $oArtigo = new Artigo();

    $oArtigo->setData($_POST);

    $oArtigo->saveArtigo();

    header("Location: /aluno");
    exit();

});