<?php

use Hcode\Model\Artigo;
use Hcode\Model\User;
use Hcode\PageAdmin;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/admin/artigos', function() {

    User::verifyLogin();

    $voArtigos = Artigo::listAll();

    $page = new PageAdmin();

    $page->setTpl("artigos", array(
        "voArtigos"=>$voArtigos
    ));

});