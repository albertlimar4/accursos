<?php

use Hcode\Model\Payment;

/** @noinspection PhpUndefinedVariableInspection */
$app->post('/pagseguro/session', function() {

    $oPay = new Payment();

    $oPay->getSessionId();

});

$app->get('/pagseguro/notificacao', function() {

    $oPay = new Payment();

//    $vTransaction = $oPay->getTransaction('B1EA99146A464A77AA84F4870C4CC928');
    $voPagamento = $oPay->listAll();

    foreach ($voPagamento as $oPagamento) {
        $vTransaction = $oPay->getTransaction($oPagamento['pgo_cod']);
        $oPay->updateTransaction($vTransaction->status,$vTransaction->code);
    }
    echo "Feito";

});

$app->post('/pagseguro/pagamento', function() {

    $oPay = new Payment();

    $oPay->setData($_POST);

    $oPay->createTransaction();

//    $oPay->getCallback();

});