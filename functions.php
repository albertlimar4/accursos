<?php

use Hcode\Model\Menu;
use Hcode\Model\Pessoa;
use Hcode\Model\User;

function getUserName(){

    $sNome = strtolower(mask_nome($_SESSION[User::SESSION]['PfiNome'])." ".mask_sobrenome($_SESSION[User::SESSION]['PfiNome']));

    return mb_convert_case($sNome,2,'UTF-8');

}

function formataValor($nValor){

    $nValor = str_replace("R$ ", "", $nValor);
    $nValor = str_replace(",", ".", $nValor);

    return $nValor;

}

function getAnoAtual(){
    return date("Y");
}

function getUserCadastro(){
    setlocale(LC_ALL, NULL);
    setlocale(LC_ALL, 'pt_BR');

    return ucfirst(gmstrftime('%b.%Y', strtotime($_SESSION[User::SESSION]['UsoCadastro'])));

}

function getUserPhoto(){

    return $_SESSION[User::SESSION]['FotUrl'];

}

function getUserRole(){

    return $_SESSION[User::SESSION]['UroId'];

}

function getMenuAcesso(){

    $nUroId = $_SESSION[User::SESSION]['UroId'];

    $voMenu = Menu::montaMenu($nUroId);

    $voResult = '';

    foreach($voMenu as $oMenuPai){

        $sMenuOpen = (array_key_exists('sub_menu',$oMenuPai)) ? "class='treeview menu-open'" : "";
        $sLink = (array_key_exists('sub_menu',$oMenuPai)) ? '#' : $oMenuPai['men_link'];

        $voResult .= "<li {$sMenuOpen}><a href='{$sLink}'><i class='{$oMenuPai['men_icone']}'></i><span>{$oMenuPai['men_descricao']}</span>";

        $voResult .= (array_key_exists('sub_menu',$oMenuPai)) ? "<span class='pull-right-container'><i class='fa fa-angle-left pull-right'></i></span>" : "";

        $voResult .= "</a>";

        if (array_key_exists('sub_menu',$oMenuPai)){
            $voResult .= "<ul class='treeview-menu'>";

            $voResult .= "<li><a href='{$oMenuPai['men_link']}'><i class='{$oMenuPai['men_icone']}'></i><span>{$oMenuPai['men_descricao']}</span></a></li>";

            foreach ($oMenuPai['sub_menu'] as $oMenuFilho) {
                $voResult .= "<li><a href='{$oMenuFilho['men_link']}'><i class='{$oMenuFilho['men_icone']}'></i><span>{$oMenuFilho['men_descricao']}</span></a></li>";
            }

            $voResult .= "</ul>";

        }

        $voResult .= "</li>";
    }

    return $voResult;

}

function situacaoMatricula($nProgresso, $nMedia){
    if ($nProgresso >= 75 && $nMedia >= 7){
        $sSituacao = "Concluído";
    } else {
        $sSituacao = "Cursando";
    }

    return $sSituacao;
}

function post($key)
{
    return str_replace("'", "", $_POST[$key]);
}
function get($key)
{
    return str_replace("'", "", $_GET[$key]);
}

function mask_nome($nome){
    $nome = explode(" ", $nome);

    return $nome[0];
}

function mask_sobrenome($nome){
    $sobrenome = explode(" ", $nome);
    $sobrenome = array_reverse($sobrenome);

    return $sobrenome[0];
}

function removeAcentos($str) {
    $str = mb_strtolower($str,'utf-8');
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
//    $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '', $str);
    //$str = preg_replace('/[^a-z0-9]/i', '_', $str);
    $str = preg_replace('/_+/', '_', $str); // ideia do Bacco :)
    return $str;
}

function removeEspacos($string) {
    $string = str_replace(" ", "_", $string);

    return $string;
}

function limitarTexto($texto, $limite){
    $texto = substr($texto, 0, $limite) . '...';
    return $texto;
}

function maskcpf($cpf){
    if (empty($cpf)) {
        return false;
    }
    $cpf = trim($cpf);
    $cpf = str_replace(" ", "", $cpf);

    $ponto = strripos($cpf, ".");
    if($ponto == false){
        $parte_um = substr($cpf, 0, 3);
        $parte_dois = substr($cpf, 3, 3);
        $parte_tres = substr($cpf, 6, 3);
        $parte_quatro = substr($cpf, 9, 2);

        $montacpf = "$parte_um.$parte_dois.$parte_tres-$parte_quatro";
    }

    elseif(is_string($cpf)){
        $montacpf = $cpf;
    }

    else{
        $montacpf = $cpf;
    }
    return $montacpf;
}

function mask_inv_cpf($cpf){
    $cpf = str_replace(".", "", $cpf);
    $cpf = str_replace("-", "", $cpf);

    return $cpf;
}

function formatDecimal($vlprice){

    if (!$vlprice > 0) $vlprice = 0;
    $vlprice = str_replace(".", "", $vlprice);

    return number_format((float)$vlprice, 2, ".", "");

}

function maskData($sDate, $bTime = false){
    try {
        $oDate = DateTime::createFromFormat(($bTime) ? "d/m/Y H:i" : "d/m/Y",$sDate);
        return ($bTime) ? $oDate->format("Y-m-d H:i:s") : $oDate->format("Y-m-d");
    } catch (Exception $e){
        return $e->getMessage();
    }
}

function maskInvData($sDate,$bTime = false){
    try {
        $oDate = new DateTime($sDate);
        return ($bTime) ? $oDate->format('d/m/Y H:i') : $oDate->format('d/m/Y');
    } catch (Exception $e) {
        return $e->getMessage();
    }
}

function formatPrice($vlprice){

    if (!$vlprice > 0) $vlprice = 0;

    return number_format((float)$vlprice, 2, ",", ".");

}

function getBrowser() {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
        '/msie/i'       =>  'Internet Explorer',
        '/firefox/i'    =>  'Firefox',
        '/safari/i'     =>  'Safari',
        '/chrome/i'     =>  'Chrome',
        '/edge/i'       =>  'Edge',
        '/opera/i'      =>  'Opera',
        '/netscape/i'   =>  'Netscape',
        '/maxthon/i'    =>  'Maxthon',
        '/konqueror/i'  =>  'Konqueror',
        '/mobile/i'     =>  'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }

    }

    return $browser;

}

function getOS() {

    $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform    =   "Unknown OS Platform";

    $os_array       =   array(
        '/windows nt 10/i'     =>  'Windows 10',
        '/windows nt 6.3/i'     =>  'Windows 8.1',
        '/windows nt 6.2/i'     =>  'Windows 8',
        '/windows nt 6.1/i'     =>  'Windows 7',
        '/windows nt 6.0/i'     =>  'Windows Vista',
        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
        '/windows nt 5.1/i'     =>  'Windows XP',
        '/windows xp/i'         =>  'Windows XP',
        '/windows nt 5.0/i'     =>  'Windows 2000',
        '/windows me/i'         =>  'Windows ME',
        '/win98/i'              =>  'Windows 98',
        '/win95/i'              =>  'Windows 95',
        '/win16/i'              =>  'Windows 3.11',
        '/macintosh|mac os x/i' =>  'Mac OS X',
        '/mac_powerpc/i'        =>  'Mac OS 9',
        '/linux/i'              =>  'Linux',
        '/ubuntu/i'             =>  'Ubuntu',
        '/iphone/i'             =>  'iPhone',
        '/ipod/i'               =>  'iPod',
        '/ipad/i'               =>  'iPad',
        '/android/i'            =>  'Android',
        '/blackberry/i'         =>  'BlackBerry',
        '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value) {

        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }

    }

    return $os_platform;

}

function getErrorFile($code){

    switch ($code){
        case(1):
        case(2):
            return "O arquivo enviado excede o tamanho máximo permitido.";
            break;

        case(3):
            return "O upload do arquivo foi feito parcialmente.";
            break;

        case(4):
            return "Nenhum arquivo foi enviado.";
            break;

        case(6):
            return "Não foi possível enviar o arquivo.";
//            return "Pasta temporária ausente.";
            break;

        case(7):
            return "Não foi possível enviar o arquivo.";
//            return "Falha em escrever o arquivo em disco.";
            break;

        case(8):
            return "Não foi possível enviar o arquivo.";
//            return "Uma extensão do PHP interrompeu o upload do arquivo.";
            break;

        default:
            return "Não foi possível enviar o arquivo.";
    }


}

function By2M($size){
    $filesizename = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
}

function somarData($data, $dias, $meses, $anos) {
    $data = explode("-", $data);

    $resData = date("Y-m-d", mktime(0,0,0,$data[1]+$meses,$data[2]+$dias,$data[0]+$anos));

    return $resData;
}

function subtraiData($data, $dias, $meses, $anos) {
    $data = explode("-", $data);

    $resData = date("Y-m-d", mktime(0,0,0,$data[1]-$meses,$data[2]-$dias,$data[0]-$anos));

    return $resData;
}

function recursive_show_array($arr){
    $nivel = 1;
    $result = "<ul>";
    foreach($arr as $value) {
        if(is_array($value)) {
            recursive_show_array($value);
        }
        else{
            echo "<li>".$nivel."=>".$value."</li>";
            $nivel +=1;
        }
    }
    $result .= "</ul>";
    return $result;
}

function make_list($arr) {
    $return = "<ul>";
    foreach ($arr as $item) {
        if (!empty($item['children'])) {
            $return .= "<li>" . $item['titulo'];
            $return .= make_list($item['children']);
            $return .= "</li>";
        } else {
            $return .= "<li>" . $item['id'] . "</li>";
        }
    }
    $return .= "</ul>";
    return $return;
}

function gravar($texto){
    //Variável arquivo armazena o nome e extensão do arquivo.
    $arquivo = "notificacao.txt";

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "a+");

    //Escreve no arquivo aberto.
    fwrite($fp, $texto);

    //Fecha o arquivo.
    fclose($fp);
}

function ler(){
    //Variável arquivo armazena o nome e extensão do arquivo.
    $arquivo = "meu_arquivo.txt";

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "r");

    //Lê o conteúdo do arquivo aberto.
    while (!feof ($fp)) {
        $valor = fgets($fp, 4096);

        echo $valor."<br>";
    }
    //Fecha o arquivo.
    fclose($fp);

    //retorna o conteúdo.
}