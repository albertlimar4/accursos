<?php

use Hcode\Model\Evento;
use Hcode\PageCongresso;
use Hcode\PageSite;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/', function() {

    $voEventoDestaque = Evento::listEventosDestaque();
    $voEvento = Evento::listEventosSite();

    $page = new PageSite();

    $page->setTpl("site", [
        "voEvento"=>$voEvento,
        "voEventoDestaque"=>$voEventoDestaque,
        "dHoje"=>date("Y-m-d")
    ]);

});

$app->get('/institucional', function() {

    $page = new PageSite();

    $page->setTpl("institucional");

});

$app->get('/cursos', function() {

    header("Location: /");
    exit();

});

$app->get('/formacoes', function() {

    header("Location: /");
    exit();

});

$app->get('/eventos', function() {

    header("Location: /");
    exit();

});

$app->get('/galeria', function() {

    header("Location: /");
    exit();

});

$app->get('/responsabilidade-social', function() {

    header("Location: /");
    exit();

});

$app->get('/contato', function() {

    header("Location: /");
    exit();

});

$app->get('/meetingparaibano', function() {

    $page = new PageCongresso([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("index");

});