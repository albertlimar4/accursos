<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Menu;

/** @noinspection PhpUndefinedVariableInspection */
$app->get('/admin/menu', function() {

    User::verifyLogin();

    $voMenu = Menu::menuListAll();

    $page = new PageAdmin();

    $page->setTpl("menu", array(
        "voMenu"=>$voMenu
    ));

});

$app->post('/admin/menu/update/ordem', function() {

    User::verifyLogin();

    $nOrdem = 1;

    foreach ($_POST['menu'] as $nMenuId){
        Menu::menuOrdem($nOrdem, $nMenuId);
        $nOrdem ++;
    }

});

$app->get('/admin/menu/create', function() {

    User::verifyLogin();

    $voMenu = Menu::menuListAll();

    $page = new PageAdmin();

    $page->setTpl("menu-create", [
        "voMenu"=>$voMenu
    ]);

});

$app->post('/admin/menu/role', function() {

    User::verifyLogin();

    $Menu = new Menu();

    $Menu->setData($_POST);

    $Menu->saveMenuRole();

    header("Location: /admin/roles");
    exit();

});

$app->get('/admin/menu/:nMenId/delete', function($nMenId) {

    User::verifyLogin();

    $oMenu = new Menu();

    $oMenu->get((int)$nMenId);

    $oMenu->delete();

    header("Location: /admin/menu");
    exit();

});

$app->get('/admin/menu/:nMenId', function($nMenId) {

    User::verifyLogin();

    $voMenu = Menu::menuListAll();

    $oMenu = new Menu();

    $oMenu->get((int)$nMenId);

    $page = new PageAdmin();

    $page->setTpl("menu-update", array(
        "oMenu"=>$oMenu->getValues(),
        "voMenu"=>$voMenu
    ));

});

$app->post('/admin/menu/create', function (){

    User::verifyLogin();

    $oMenu = new Menu();

    $oMenu->setData($_POST);

    $oMenu->menuSave();

    header("Location: /admin/menu");
    exit();

});

$app->post('/admin/menu/update', function() {

    User::verifyLogin();

    $oMenu = new Menu();

    $oMenu->setData($_POST);

    $oMenu->menuUpdate();

    header("Location: /admin/menu");
    exit();

});