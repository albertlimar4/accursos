<?php

/** @noinspection PhpUndefinedVariableInspection */

use Hcode\Model;
use Hcode\Model\Auxiliar;
use Hcode\Model\Evento;
use Hcode\Model\EventoCategoria;
use Hcode\Model\EventoValor;
use Hcode\Model\User;
use Hcode\PageAdmin;

$app->get('/admin/eventos', function() {

    User::verifyLogin();

    $page = new PageAdmin();

    $voFormaPgto = Auxiliar::listFormaPagamento();

    $page->setTpl("eventos", array(
        "vError"=>Model::getError(),
        "voFormaPgto"=>$voFormaPgto
    ));

});

$app->get('/admin/evento/lista', function() {

    User::verifyLogin();

    $voEvento = Evento::listEventosPrincipais();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-lista", array(
        "voEvento"=>$voEvento,
        "subEvento"=>true
    ));

});

$app->get('/admin/evento/subeventos/:nEventoPai', function($nEventoPai) {

    User::verifyLogin();

    $oEvento = new Evento();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-sub-lista", array(
        "voEvento"=>$oEvento->listSubEventos($nEventoPai),
        "subEvento"=>false
    ));

});

$app->get('/admin/evento/create', function() {

    User::verifyLogin();

    $voCategoria = EventoCategoria::listAll();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-create", [
        "oEvento"=>(isset($_SESSION['oEvento'])) ? $_SESSION['oEvento'] : null,
        "voCategoria"=>$voCategoria,
        "nEventoPai"=>null,
        "vError"=>Model::getError()
    ]);

});

$app->get('/admin/evento/create/:nEventoPai', function($nEventoPai) {

    User::verifyLogin();

    $voCategoria = EventoCategoria::listAll();

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-create", [
        "oEvento"=>(isset($_SESSION['oEvento'])) ? $_SESSION['oEvento'] : null,
        "voCategoria"=>$voCategoria,
        "nEventoPai"=>$nEventoPai,
        "vError"=>Model::getError()
    ]);

});

$app->post('/admin/evento/create', function() {

    User::verifyLogin();

    $oEvento = new Evento();

    $oEvento->setData($_POST);
    $_SESSION['oEvento'] = $_POST;

    try{
        Model::setError($oEvento->save());
        unset($_SESSION['oEvento']);
        header("Location: /admin/eventos");
    } catch (Exception $e) {
        Model::setError($e->getMessage());
        header("Location: /admin/eventos");
    }
    exit();
});

$app->get('/admin/evento/:nEveId/update', function($nEveId) {

    User::verifyLogin();

    $oEvento = new Evento();

    $voCategoria = EventoCategoria::listAll();

    $voEventoValor = EventoValor::valoresPorEvento($nEveId);

    $oEvento->get((int)$nEveId);

    $page = new PageAdmin([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("evento-update", array(
        "oEvento"=>$oEvento->getValues(),
        "voCategoria"=>$voCategoria,
        "voEventoValor"=>$voEventoValor
    ));

});

$app->post('/admin/evento/update', function() {

    User::verifyLogin();

    $oEvento = new Evento();

    $oEvento->setData($_POST);

    try{
        Model::setError($oEvento->update());
        header("Location: /admin/eventos");
    } catch (Exception $e) {
        Model::setError($e->getMessage());
        header("Location: /admin/eventos");
    }
    exit();
});

$app->get('/admin/evento/:nEveId/delete', function($nEveId) {

    User::verifyLogin();

    $oEvento = new Evento();

    $oEvento->get((int)$nEveId);

    $oEvento->delete();

    header("Location: /admin/eventos");
    exit();

});

$app->get('/admin/evento/create/:nEveId', function($nEveId) {

    User::verifyLogin();

    $oEvento = new Evento();

    $oEvento->get($nEveId);

    $voCategoria = EventoCategoria::listAll();

    $page = new PageAdmin();

    $page->setTpl("evento-create", [
        "voCategoria"=>$voCategoria,
        "oEvento"=>$oEvento->getValues()
    ]);

});

$app->post('/admin/evento/update', function($nEveId) {

    User::verifyLogin();

    $oEvento = new Evento();

    $oEvento->get((int)$nEveId);

    $oEvento->setData($_POST);

    $oEvento->update();

    header("Location: /admin/eventos");
    exit();

});
