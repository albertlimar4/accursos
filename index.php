<?php

use Hcode\Model;
use Hcode\Model\Auxiliar;
use Hcode\Model\Evento;
use Hcode\Model\Inscricao;
use Hcode\Page;
use Slim\Slim;

header("Access-Control-Allow-Origin: *");
require_once("vendor/autoload.php");

$app = new Slim();

$app->config('debug', true);

require_once "functions.php";
require_once "t-admin.php";
require_once "t-admin-alunos.php";
require_once "t-admin-evento.php";
require_once "t-admin-evento-categoria.php";
require_once "t-admin-evento-inscricao.php";
require_once "t-admin-menu.php";
require_once "t-admin-pagseguro.php";
require_once "t-admin-pessoa.php";
require_once "t-admin-roles.php";
require_once "t-admin-users.php";

require_once "t-aluno.php";

require_once "t-aux.php";

require_once "t-site.php";

$app->post('/inscricao/save', function() {
    session_start();

    $oInscricao = new Inscricao();

    $oInscricao->setData($_POST);

    $nEventoId = $_POST['EveId'];

    try {

        $_SESSION['oInscricao'] = $oInscricao->save();
        header("Location: /inscricao/checkout");
        exit();

    } catch (Exception $e) {

        $_SESSION['oInscricao'] = $_POST;
        Model::setError($e->getMessage());
        header("Location: /inscricao/$nEventoId");
        exit();
    }

});

$app->get('/inscricao/sucesso', function() {
    session_start();

    $page = new Page();

    $page->setTpl("sucesso", array(
        "oInscricao"=>$_SESSION['oInscricao']
    ));

});

$app->get('/inscricao/checkout', function() {
    session_start();

    $page = new Page();

    $page->setTpl("checkout", array(
        "oInscricao"=>$_SESSION['oInscricao']
    ));

});

$app->get('/inscricao/:nEveId', function($nEveId) {
    session_start();

    $voEscolaridade = Auxiliar::listEscolaridade();

    $voEstado = Auxiliar::listEstado();

    $voCidade = Auxiliar::listCidade();

    $oEvento = new Evento();

    $oEvento->get($nEveId);

    $page = new Page();

    $page->setTpl("inscricao", array(
        "oEvento"=>$oEvento->getValues(),
        "voEscolaridade"=>$voEscolaridade,
        "bInscricao"=>($oEvento->getEveFim() < date("Y-m-d")) ? false : true,
        "voCidade"=>$voCidade,
        "voEstado"=>$voEstado,
        "vError"=>Model::getError(),
        "vDados"=>(isset($_SESSION['oInscricao'])) ? $_SESSION['oInscricao'] : ['EveId'=>$nEveId,'PfiNome'=>'', 'PfiCpf'=>'', 'PfiSexo'=>'', 'TelDdd'=>'', 'TelNumero'=>'', 'PesEmail'=>'', 'EndCep'=>'', 'EndRua'=>'', 'EndNumero'=>'', 'EndComplemento'=>'', 'EndBairro'=>'', 'EndCidade'=>'', 'EndEstado'=>'', 'categoria'=>'', 'area'=>'', 'cpf'=>'']
    ));

});

$app->run();